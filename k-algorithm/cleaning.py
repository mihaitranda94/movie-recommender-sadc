import pandas as pd
import json

#importing datasets
movies = pd.read_csv('tmdb_5000_movies.csv')
credits = pd.read_csv('tmdb_5000_credits.csv')
movies['directors'] = ""

#testing or printing
print(movies.head(1))
# print(credits.head(5))

# changing the genres column from json to string
movies['genres'] = movies['genres'].apply(json.loads)
for index,i in zip(movies.index,movies['genres']):
    list1 = []
    for j in range(len(i)):
        list1.append((i[j]['name'])) # the key 'name' contains the name of the genre
    movies.loc[index,'genres'] = str(list1)

#testing or printing
print('example of genres first 5 rows\n')
print(movies['genres'].head(5))


#In a similar fashion, we will convert the JSON to a list of strings for the columns: keywords, production_companies, cast, and crew. 
#Keywords
movies['keywords'] = movies['keywords'].apply(json.loads)
for index,i in zip(movies.index,movies['keywords']):
    list1 = []
    for j in range(len(i)):
        list1.append((i[j]['name'])) # the key 'name' contains the name of the genre
    movies.loc[index,'keywords'] = str(list1)

#testing or printing
print('example of keywords first 5 rows\n')
print(movies['keywords'].head(5))

#Production Companies
movies['production_companies'] = movies['production_companies'].apply(json.loads)
for index,i in zip(movies.index,movies['production_companies']):
    list1 = []
    for j in range(len(i)):
        list1.append((i[j]['name'])) # the key 'name' contains the name of the genre
    movies.loc[index,'production_companies'] = str(list1)

#testing or printing
print('example of production_companies first 5 rows\n')
print(movies['production_companies'].head(5))

#Cast
credits['cast'] = credits['cast'].apply(json.loads)
for index,i in zip(credits.index,credits['cast']):
    list1 = []
    for j in range(len(i)):
        list1.append((i[j]['name'])) # the key 'name' contains the name of the actor
    credits.loc[index,'cast'] = str(list1)

#testing or printing
print('example of cast first 5 rows\n')
print(credits['cast'].head(5))

#Crew
credits['crew'] = credits['crew'].apply(json.loads)
for index,i in zip(credits.index,credits['crew']):
    list_director = []
    list1 = []
    for j in range(len(i)):
        if(i[j]['job'] == 'Director') :
            # dir = ((i[j]['name'])).encode("ascii", "ignore")
            # dir_decode = dir.decode()
            list_director.append(i[j]['name'])
        list1.append(((i[j]['name'])).replace("u'", "'")) # the key 'name' contains the name of the actor
    credits.loc[index,'crew'] = str(list1)
    credits.loc[index,'directors'] = str(list_director)

#testing or printing
print('example of crew first 5 rows\n')
print(credits.head(7))

#merging
movies = movies.merge(credits, left_on='id', right_on='movie_id', how='left')
movies = movies[['id', 'original_title', 'genres', 'cast', 'vote_average', 'keywords','directors_y']]
movies.columns = ['id', 'original_title', 'genres', 'cast', 'vote_average', 'keywords','directors']

#print or test
print(movies.columns)
print(movies.head(5))

movies['directors'] = movies['directors'].astype(str)
print(movies.head(5))

movies.to_csv('cleaned_dataset.csv', index=False, encoding='ascii')