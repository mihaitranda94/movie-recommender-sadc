import pandas as pd
import json
import matplotlib.pyplot as plt
import numpy as np


def binary(genre_list):
    binaryList = []
    
    for genre in genreList:
        if genre in genre_list:
            binaryList.append(1)
        else:
            binaryList.append(0)
    
    return binaryList

def binaryCast(cast_list, cast_bin):
    binaryList = []
    # print(cast_bin[0])
    # print(cast_list)
    for genre in cast_bin:
       
        if genre in cast_list:
            binaryList.append(1)
        else:
            binaryList.append(0)
    
    return binaryList


def binary2(list_list):
    binaryList = []
    # print(words_list[0])
    # print(list_list)
    for genre in words_list:
        if genre in list_list:
            binaryList.append(1)
        else:
            binaryList.append(0)
    
    return binaryList

def binary3(list_list):
    binaryList = []
    for genre in directorList:

        if genre in list_list:
            binaryList.append(1)
        else:
            binaryList.append(0)

    # print(binaryList)
    return binaryList

def binary4(list_list):
    binaryList = []
    for genre in cast_bin:

        if genre in list_list:
            binaryList.append(1)
        else:
            binaryList.append(0)

    # print(binaryList)
    return binaryList

def zerolistmaker(n):
    listofzeros = [0] * n
    return listofzeros

def turn_to_list(str):
   
    str =str.replace('[','')
    str =str.replace(']','')
    list = str.split(',')
    return list
def turn_to_list2(str):
    str = str.strip('[]')
    str = str.replace(' ','')
    str = str.replace("'",'')
    return str.split(',')

def turn_to_list_special(str):
    str = str.strip('[]')
    str = str.replace("'",'')
    return str.split(',')

print('...encoding')
movies = pd.read_csv('cleaned_dataset.csv')
genreList = ['Action', 'Adventure', 'Fantasy', 'Science Fiction', 'Crime', 'Drama', 'Thriller', 'Animation', 'Family', 'Western']
#hot encoding for genres --done
print('encoding genres...')
movies['genres_bin'] = movies['genres'].apply(lambda x: binary(x))
print('encoding genres done!')

# hot encoding for cast -- done
print('encoding cast...')
cast_bin = []
for i,j in zip(movies['cast'],movies.index):
    actors = turn_to_list(i)
    list2 = actors[:4]
    list2  =sorted(list2)
    for act in list2:
        if act not in cast_bin:
            cast_bin.append(act)
    movies.loc[j,'cast'] = str(list2)
movies['cast_bin'] = ""
for i,j in zip(movies['cast'],movies.index):
    movies.loc[j,'cast_bin'] = str(binary4(i))
print('encoding cast done!')

#hot encoding for directors -- done
print('encoding directors...')
directorList=[]
movies['directors_bin'] = ""
for i in movies['directors']:
    if i not in directorList:
        directorList.append(i)
for i,j in zip(movies['directors'],movies.index):
    movies.loc[j,'directors_bin'] = str(binary3(i))

print('encoding directors done!')
# encoding for keywords
words_list = []
movies['words_bin'] = ""
for i,j in zip(movies['keywords'],movies.index):
    list2 = []
    list2 = turn_to_list2(i)
    list2.sort()
    for keyword in list2:
        if keyword not in words_list:
            words_list.append(keyword)
    movies.loc[j,'keywords'] = str(list2)
for i,j in zip(movies['keywords'],movies.index):
    keywords = turn_to_list2(i)
    movies.loc[j,'words_bin'] = str(binary2(keywords))

movies = movies[(movies['vote_average']!=0)]
movies = movies[movies['directors']!='']


movies.to_csv('encoded_dataset.csv', index=False, encoding='ascii')
print(movies.head(5))
print(cast_bin[0])
    


