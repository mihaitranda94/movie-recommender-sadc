
CREATE TABLE nameBasics(
	nconst 		TEXT	PRIMARY KEY,
	primaryName	TEXT	NOT NULL,
	birthYear	TEXT	NOT NULL,
	deathYear	TEXT	NOT NULL,
	primaryProfession	TEXT[]	NOT NULL,
	knownForTitles TEXT[]	NOT NULL
	);