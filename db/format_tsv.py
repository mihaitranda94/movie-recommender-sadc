import pandas as pd
import sys

# names of files to read from
if len(sys.argv) == 2:
    r_filenameTSV = sys.argv[1]
else:
    exit(1)

# names of files to write to
w_filenameTSV = '/tmp/name.basics.new.tsv'

# read the data
tsv_read = pd.read_csv(r_filenameTSV, sep='\t')

# print the first 10 records
print(tsv_read.head(10))

for index,row in tsv_read.iterrows():
    row[4] = '{' + str(row[4]) + '}'
    row[5] = '{' + str(row[5]) + '}'
    
print(tsv_read.head(10))

with open(w_filenameTSV,'w') as write_tsv:
    write_tsv.write(tsv_read.to_csv(sep='\t', index=False))