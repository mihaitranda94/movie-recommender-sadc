const express = require("express");
const router = express.Router();
// const bcrypt = require("bcrypt");
const pool = require("../../db");
const jwtGenerator = require("../jwtGenerator");
// const authorize = require("../middleware/authorization");
const spawn = require("child_process").spawn;


router.post("/register", async (req, res) => {
  //destructure req.body (name, email, password)
  const { first_name, last_name, email, password, age, country, favourite_movies, favourite_cast} = req.body;
  try {
    //check if user exists
    const user = await pool.query("SELECT * FROM primary_users WHERE email = $1", [
      email,
    ]);
    if (user.rows.length > 0) {
      return res.status(401).json("User already exist!");
    }

    let newUser = await pool.query(
      "INSERT INTO primary_users (first_name, last_name, email, password) VALUES ($1, $2, $3, $4) RETURNING *",
      [first_name, last_name, email, password]
    );
    
    let newinfo = await pool.query(
        "INSERT INTO users_informations (email, age, country, favourite_movies, favourite_cast) VALUES ($1, $2, $3, $4, $5) RETURNING *",
        [ email, age, country, favourite_movies, favourite_cast]
      );
    res.send(newinfo.rows);
  } catch (err) {
    console.error(err.message);
    res.status(500).send(err.message);
  }
});

router.post("/login", async (req, res) => {
    const { email, password } = req.body;
    try {
      const user = await pool.query("SELECT * FROM primary_users WHERE email = $1", [
        email,
      ]);
     
      if (user.rows.length == 0) {
        return res.status(401).json("Invalid Credential");
      }
  
      const validPassword = password == user.rows[0].password ? true: false;
      if (!validPassword) {
        console.log(user.rows[0].password );
        console.log(password );

        return res.status(403).json("Invalid Credential");
        
      }
    //   const jwtToken = jwtGenerator(user.rows[0].user_id);
    console.log(validPassword)
    const jwtToken = jwtGenerator(user.rows[0].user_id);
    return res.json({ jwtToken });
    } catch (err) {
      console.error(err.message);
      res.status(500).json("Server error");
    }
  });

  router.post("/cast/:email", async (req, res) => {
    const email = req.params.email;
    console.log(email);
    try {
      const user = await pool.query('SELECT * FROM users_informations WHERE email = $1', [
        email,
      ]);
      console.log(user);
      if (user.rows.length == 0) {
        return res.status(401).json("No User like that");
      }
  
  
      return res.send(user.rows[0].favourite_cast);
    } catch (err) {
      console.error(err.message);
      res.status(500).json("Server error");
    }
  });

  router.post("/movies/:email", async (req, res) => {
    const email = req.params.email;
    console.log(email);
    try {
      const user = await pool.query('SELECT * FROM users_informations WHERE email = $1', [
        email,
      ]);
      console.log(user);
      if (user.rows.length == 0) {
        return res.status(401).json("No User like that");
      }
  
  
      return res.send(user.rows[0].favourite_movies);
    } catch (err) {
      console.error(err.message);
      res.status(500).json("Server error");
    }
  });

  router.post("/users/:email", async (req, res) => {
    const email = req.params.email;
    var movies = []
    console.log(email);
    try {
      const user = await pool.query('SELECT * FROM users_informations WHERE email = $1', [
        email,
      ]);
      // console.log(user);
      if (user.rows.length == 0) {
        return res.status(401).json("No User like that");
      }
      movies_user = user.rows[0].favourite_movies;
      const other_user = await pool.query('SELECT favourite_movies FROM users_informations WHERE email != $1', [
       email]);
      console.log('bummmmmmmmmm' + movies_user)
      var flag = 0
      var movieYouWatched;
      var prag = 0;
      for (let i = 0; i < other_user.rows.length; i++) {
        var temp = []
        for (let j=0; j< other_user.rows[i].favourite_movies.length; j++) {
          
          if (movies_user.includes(other_user.rows[i].favourite_movies[j])) {
            console.log('!!!!!!' + other_user.rows[i].favourite_movies[j])
            flag=1;
            movieYouWatched = other_user.rows[i].favourite_movies[j];
            prag = prag +1
            console.log(flag)
          } else {
            console.log('pun' + other_user.rows[i].favourite_movies[j])
            temp.push(other_user.rows[i].favourite_movies[j])
          }

         
        }

        if (flag ==1 && temp.length != 0 && prag>=2) {
          flag = 0;
          prag =0;
          console.log('flag e 1 si temp =' + temp)
          if(movies.length == 0) {
            movies = temp;
          } else {
            movies.concat(temp);
          }
        }
     

      }
      movies.push((movieYouWatched)) 
      res.send(movies)
    } catch (err) {
      console.error(err.message);
      res.status(500).json("Server error");
    }
  });
module.exports = router;



