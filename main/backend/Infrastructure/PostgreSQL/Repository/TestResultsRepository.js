const { queryAsync } = require("..");
const ServerError = require("../../../WebApp/Models/ServerError");

const addAsync = async (user_id, doc_name, data) => {
  console.info(`Adding test_results in database async...`);
  if (!user_id)
    throw new ServerError("Error in Booking_requests post in Repository", 500);
  const test_results = await queryAsync(
    "INSERT INTO test_results (user_id, doc_name, date, data) VALUES ($1, $2, now(), $3) RETURNING *",
    [user_id, doc_name, data]
  );
  return test_results[0];
};

const getAllAsync = async () => {
  console.info(`Getting all test_results from database async...`);

  return await queryAsync("SELECT * FROM test_results RETURNING *");
};

const getByIdAsync = async (r_id) => {
  console.info(`Getting the test_results with r_id ${r_id} from database async...`);

  const test_results = await queryAsync("SELECT * FROM test_results WHERE r_id = $1 RETURNING *", [
    r_id,
  ]);
  return test_results[0];
};

const getByUidAsync = async (u_id) => {
  console.info(`Getting the test_results with user_id ${u_id} from database async...`);

  const test_results = await queryAsync("SELECT * FROM test_results WHERE user_id = $1", [
    u_id,
  ]);
  return test_results;
};

const getUserByNameAndEmailAsync = async (first_name, last_name, email) => {
  //console.info(`Getting the user with name ${last_name} from database async...`);
  console.info(`asdasdasdasdsadasdasd ${first_name} ${email}\n\n\n\\n\n`);
  const test_results = await queryAsync("SELECT * FROM users WHERE first_name = $1 and last_name = $2 and email= $3"
  , [
    first_name, last_name, email
  ]);
  console.log(test_results);
  
  return test_results[0];
};

const getDoctorByNameAsync = async (name) => {
  console.info(`Getting the Doctor with name ${name} from database async...`);

  const test_results = await queryAsync("SELECT * FROM doctors WHERE name = $1"
  , [
    name
  ]);
  return test_results[0];
};

const updateStatusByIdAsync = async (r_id, user_id, doc_name, data) => {
  console.info(`Updating the test_results with r_id ${r_id} from database async...`);

  const test_results = await queryAsync(
    "UPDATE test_results SET user_id=$1, description=$2, date=$3, slot=$4, doc_id=$5, status=$6 WHERE r_id = $7 RETURNING *",
    [user_id, doc_name, data, r_id]
  );
  return test_results[0];
};

const deleteByIdAsync = async (r_id) => {
  console.info(`Deleting the author with r_id ${r_id} from database async...`);

  const test_results = await queryAsync(
    "DELETE FROM test_results WHERE r_id = $1 RETURNING *",
    [r_id]
  );
  return test_results[0];
};

module.exports = {
  addAsync,
  getAllAsync,
  getByIdAsync,
  updateStatusByIdAsync,
  deleteByIdAsync,
  getUserByNameAndEmailAsync,
  getDoctorByNameAsync,
  getByUidAsync
};
