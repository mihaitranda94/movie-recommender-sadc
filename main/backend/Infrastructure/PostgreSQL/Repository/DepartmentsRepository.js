const { queryAsync } = require("..");

const addAsync = async (name, loc_id, description) => {
  console.info(`Adding department in database`);

  const department = await queryAsync(
    "INSERT INTO department (name, loc_id, description) VALUES ($1, $2, $3) RETURNING *",
    [name, loc_id, description]
  );

  return department[0];
};

const getAllAsync = async () => {
  console.info(`Getting all department from database`);

  return await queryAsync("SELECT * FROM department");
};

const getAllByLocIdAsync = async (loc_id) => {
  console.info(`Getting all department from database`);

  return await queryAsync("SELECT * FROM department WHERE loc_id=$1", [loc_id]);
};

const deleteByIdAsync = async (id) => {
    console.info(`Deleting the department with d_id ${id} from database async...`);
  
    const department = await queryAsync(
      "DELETE FROM department WHERE dpt_id = $1 RETURNING *",
      [id]
    );
    return department[0];
  };

module.exports = {
  addAsync,
  getAllAsync,
  getAllByLocIdAsync,
  deleteByIdAsync
};
