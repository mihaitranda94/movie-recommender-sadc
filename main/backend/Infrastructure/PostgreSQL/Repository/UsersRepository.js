const { queryAsync } = require("..");

const getAllAsync = async () => {
  console.info("Getting all users from database");

  return await queryAsync("SELECT * FROM users");
};

const getUserByEmailAndPhoneAsync = async (email, phone) => {
  console.info("Getting user by email and phone from database");

  return await queryAsync(
    "SELECT * FROM users WHERE email = $1 and phone = $2",
    [email, phone]
  );
};

const addAsync = async (
  first_name,
  last_name,
  password,
  type,
  email,
  phone
) => {
  console.info(`Adding user ${first_name} ${last_name}`);

  const users = await queryAsync(
    "INSERT INTO users (first_name, last_name, created_at, password, type, email, phone) VALUES \
    ($1, $2, now(), $3, $4, $5, $6) RETURNING *",
    [first_name, last_name, password, type, email, phone]
  );
  return users[0];
};

const getByEmailWithTypeAsync = async (email) => {
  console.info(`Getting user with email ${email}`);

  const users = await queryAsync(
    `SELECT u_id, email, password, type from users where email = $1`,
    [email]
  );
  return users[0];
};

const getByIdAsync = async (u_id) => {
  console.info(`Getting the user with u_id ${u_id} from database async...`);

  const bookings_requests = await queryAsync("SELECT * FROM users WHERE u_id = $1", [
    u_id,
  ]);
  return bookings_requests[0];
};

module.exports = {
  getAllAsync,
  addAsync,
  getByEmailWithTypeAsync,
  getByIdAsync
};
