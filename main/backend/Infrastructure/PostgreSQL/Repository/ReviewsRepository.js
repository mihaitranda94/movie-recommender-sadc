const { request } = require("express");
const { queryAsync } = require("..");
const ServerError = require("../../../WebApp/Models/ServerError");

const addAsync = async (user_id, doc_id, rating, message) => {
  console.info(`Adding reviews in database async...`);
  if (!user_id)
    throw new ServerError("Error in reviews post in Repository", 500);
  const reviews = await queryAsync(
    "INSERT INTO reviews (user_id, doc_id, rating, message) VALUES ($1, $2, $3, $4) RETURNING *",
    [user_id, doc_id, rating, message]
  );
  return reviews[0];
};

const getAllAsync = async (id) => {
  console.info(`Getting all reviews from database async...`);

  return await queryAsync("SELECT * FROM reviews where doc_id=$1", [id]);
};

// const getByIdAsync = async (req_id) => {
//   console.info(`Getting the reviews with req_id ${req_id} from database async...`);

//   const reviews = await queryAsync("SELECT * FROM reviews WHERE req_id = $1", [
//     req_id,
//   ]);
//   return reviews[0];
// };

// const getUserByNameAndEmailAsync = async (first_name, last_name, email) => {
//   //console.info(`Getting the user with name ${last_name} from database async...`);
//   console.info(`asdasdasdasdsadasdasd ${first_name} ${email}\n\n\n\\n\n`);
//   const reviews = await queryAsync("SELECT * FROM users WHERE first_name = $1 and last_name = $2 and email= $3"
//   , [
//     first_name, last_name, email
//   ]);
//   console.log(reviews);

//   return reviews[0];
// };

// const getDoctorByNameAsync = async (name) => {
//   console.info(`Getting the Doctor with name ${name} from database async...`);

//   const reviews = await queryAsync("SELECT * FROM doctors WHERE name = $1"
//   , [
//     name
//   ]);
//   return reviews[0];
// };

// const updateByIdAsync = async (req_id, user_id, description, createdAt, date, slot, doc_id) => {
//   console.info(`Updating the reviews with req_id ${req_id} from database async...`);

//   const reviews = await queryAsync(
//     "UPDATE reviews SET name = $1, address=$2 WHERE req_id = $3 RETURNING *",
//     [user_id, description, createdAt, date, slot, doc_id, req_id]
//   );
//   return reviews[0];
// };

const deleteByIdAsync = async (rev_id) => {
  console.info(
    `Deleting the author with rev_id ${rev_id} from database async...`
  );

  const reviews = await queryAsync(
    "DELETE FROM reviews WHERE rev_id = $1 RETURNING *",
    [rev_id]
  );
  return reviews[0];
};

module.exports = {
  addAsync,
  getAllAsync,
  //   getByIdAsync,
  //   updateByIdAsync,
  deleteByIdAsync,
  //   getUserByNameAndEmailAsync,
  //   getDoctorByNameAsync
};
