const { queryAsync } = require("..");

const addAsync = async (name, dep_id, loc_id) => {
  console.info(`Adding doctor in database async...`);

  const doctors = await queryAsync(
    "INSERT INTO doctors (name, dep_id, loc_id) VALUES ($1, $2, $3) RETURNING *",
    [name, dep_id, loc_id]
  );
  return doctors[0];
};

const getAllAsync = async () => {
  console.info(`Getting all doctors from database async...`);

  return await queryAsync("SELECT * FROM doctors");
};

const getAllByDepAndLocAsync = async (dep_id) => {
  console.info(`Getting doctors with dep ${dep_id} from database async...`);

  return await queryAsync(
    "SELECT * FROM doctors where dep_id = $1",
    [dep_id]
  );
};

const getByIdAsync = async (id) => {
  console.info(`Getting the doctor with d_id ${id} from database async...`);

  const doctors = await queryAsync("SELECT * FROM doctors WHERE d_id = $1", [
    id,
  ]);
  return doctors[0];
};

const updateByIdAsync = async (id, name, dep_id, loc_id) => {
  console.info(`Updating the doctor with d_id ${id} from database async...`);

  const doctors = await queryAsync(
    "UPDATE doctors SET name = $1, dep_id = $2, loc_id = $3 WHERE d_id = $4 RETURNING *",
    [name, dep_id, loc_id, id]
  );
  return doctors[0];
};

const deleteByIdAsync = async (id) => {
  console.info(`Deleting the doctor with d_id ${id} from database async...`);

  const doctors = await queryAsync(
    "DELETE FROM doctors WHERE d_id = $1 RETURNING *",
    [id]
  );
  return doctors[0];
};

const getLocationByDoctorId = async (id) => {
  console.info(
    `Getting the location of the doctor with d_id ${id} from database async...`
  );

  const doctors = await queryAsync(
    "select l.l_id, l.name, l.address FROM doctors d, location l  WHERE d.d_id = $1 and d.loc_id = l.l_id",
    [id]
  );
  return doctors[0];
};

const getDepartmentByDoctorId = async (id) => {
  console.info(
    `Getting the department of the doctor with d_id ${id} from database async...`
  );

  const doctors = await queryAsync(
    "select dp.dpt_id, dp.name, dp.description FROM doctors d, department dp  WHERE d.d_id = $1 and d.dep_id = dp.dpt_id",
    [id]
  );
  return doctors[0];
};

module.exports = {
  addAsync,
  getAllAsync,
  getAllByDepAndLocAsync,
  getByIdAsync,
  updateByIdAsync,
  deleteByIdAsync,
  getLocationByDoctorId,
  getDepartmentByDoctorId,
};
