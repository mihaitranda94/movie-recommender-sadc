const { request } = require("express");
const { queryAsync } = require("..");
const ServerError = require("../../../WebApp/Models/ServerError");

const addAsync = async (user_id, description, date, slot, doc_id, request_id) => {
  console.info(`Adding bookings in database async...`);
  if (!user_id)
    throw new ServerError("Error in Booking_requests post in Repository", 500);
  const bookings = await queryAsync(
    "INSERT INTO bookings (user_id, description, created_at, date, slot, doc_id, request_id) VALUES ($1, $2, now(), $3, $4, $5, $6) RETURNING *",
    [user_id, description, date, slot, doc_id, request_id]
  );
  return bookings[0];
};

const getAllAsync = async () => {
  console.info(`Getting all bookings from database async...`);

  return await queryAsync("SELECT * FROM bookings");
};

const getByIdAsync = async (req_id) => {
  console.info(`Getting the bookings with req_id ${req_id} from database async...`);

  const bookings = await queryAsync("SELECT * FROM bookings WHERE req_id = $1", [
    req_id,
  ]);
  return bookings[0];
};

const getUserByNameAndEmailAsync = async (first_name, last_name, email) => {
  //console.info(`Getting the user with name ${last_name} from database async...`);
  console.info(`asdasdasdasdsadasdasd ${first_name} ${email}\n\n\n\\n\n`);
  const bookings = await queryAsync("SELECT * FROM users WHERE first_name = $1 and last_name = $2 and email= $3"
  , [
    first_name, last_name, email
  ]);
  console.log(bookings);
  
  return bookings[0];
};

const getDoctorByNameAsync = async (name) => {
  console.info(`Getting the Doctor with name ${name} from database async...`);

  const bookings = await queryAsync("SELECT * FROM doctors WHERE name = $1"
  , [
    name
  ]);
  return bookings[0];
};

const updateByIdAsync = async (req_id, user_id, description, createdAt, date, slot, doc_id) => {
  console.info(`Updating the bookings with req_id ${req_id} from database async...`);

  const bookings = await queryAsync(
    "UPDATE bookings SET name = $1, address=$2 WHERE req_id = $3 RETURNING *",
    [user_id, description, createdAt, date, slot, doc_id, req_id]
  );
  return bookings[0];
};

const deleteByIdAsync = async (req_id) => {
  console.info(`Deleting the author with req_id ${req_id} from database async...`);

  const bookings = await queryAsync(
    "DELETE FROM bookings WHERE req_id = $1 RETURNING *",
    [req_id]
  );
  return bookings[0];
};

module.exports = {
  addAsync,
  getAllAsync,
  getByIdAsync,
  updateByIdAsync,
  deleteByIdAsync,
  getUserByNameAndEmailAsync,
  getDoctorByNameAsync
};
