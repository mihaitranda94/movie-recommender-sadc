const { queryAsync } = require("..");

const addAsync = async (name, address) => {
  console.info(`Adding location in database async...`);

  const location = await queryAsync(
    "INSERT INTO location (name, address) VALUES ($1, $2) RETURNING *",
    [name, address]
  );
  return location[0];
};

const getAllAsync = async () => {
  console.info(`Getting all location from database async...`);

  return await queryAsync("SELECT * FROM location");
};

const getByIdAsync = async (l_id) => {
  console.info(`Getting the location with l_id ${l_id} from database async...`);

  const location = await queryAsync("SELECT * FROM location WHERE l_id = $1", [
    l_id,
  ]);
  return location[0];
};

const updateByIdAsync = async (l_id, name, address) => {
  console.info(`Updating the location with l_id ${l_id} from database async...`);

  const location = await queryAsync(
    "UPDATE location SET name = $1, address=$2 WHERE l_id = $3 RETURNING *",
    [name, address, l_id]
  );
  return location[0];
};

const deleteByIdAsync = async (l_id) => {
  console.info(`Deleting the author with l_id ${l_id} from database async...`);

  const location = await queryAsync(
    "DELETE  FROM location WHERE l_id = $1 RETURNING *",
    [l_id]
  );


  
  return location[0];
};

module.exports = {
  addAsync,
  getAllAsync,
  getByIdAsync,
  updateByIdAsync,
  deleteByIdAsync,
};
