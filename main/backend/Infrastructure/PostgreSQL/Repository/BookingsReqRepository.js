const { queryAsync } = require("..");
const ServerError = require("../../../WebApp/Models/ServerError");

const addAsync = async (user_id, description, date, slot, doc_id, status) => {
  console.info(`Adding bookings_requests in database async...`);
  if (!user_id)
    throw new ServerError("Error in Booking_requests post in Repository", 500);
  const bookings_requests = await queryAsync(
    "INSERT INTO bookings_requests (user_id, description, created_at, date, slot, doc_id, status) VALUES ($1, $2, now(), $3, $4, $5, $6) RETURNING *",
    [user_id, description, date, slot, doc_id, status]
  );
  return bookings_requests[0];
};

const getAllAsync = async () => {
  console.info(`Getting all bookings_requests from database async...`);

  return await queryAsync("SELECT * FROM bookings_requests");
};

const getByIdAsync = async (req_id) => {
  console.info(`Getting the bookings_requests with req_id ${req_id} from database async...`);

  const bookings_requests = await queryAsync("SELECT * FROM bookings_requests WHERE req_id = $1", [
    req_id,
  ]);
  return bookings_requests[0];
};

const getUserByNameAndEmailAsync = async (first_name, last_name, email) => {
  //console.info(`Getting the user with name ${last_name} from database async...`);
  console.info(`asdasdasdasdsadasdasd ${first_name} ${email}\n\n\n\\n\n`);
  const bookings_requests = await queryAsync("SELECT * FROM users WHERE first_name = $1 and last_name = $2 and email= $3"
  , [
    first_name, last_name, email
  ]);
  console.log(bookings_requests);
  
  return bookings_requests[0];
};

const getDoctorByNameAsync = async (name) => {
  console.info(`Getting the Doctor with name ${name} from database async...`);

  const bookings_requests = await queryAsync("SELECT * FROM doctors WHERE name = $1"
  , [
    name
  ]);
  return bookings_requests[0];
};

const updateStatusByIdAsync = async (req_id, user_id, description, date, slot, doc_id, status) => {
  console.info(`Updating the bookings_requests with req_id ${req_id} from database async...`);

  const bookings_requests = await queryAsync(
    "UPDATE bookings_requests SET user_id=$1, description=$2, date=$3, slot=$4, doc_id=$5, status=$6 WHERE req_id = $7 RETURNING *",
    [user_id, description, date, slot, doc_id, status, req_id]
  );
  return bookings_requests[0];
};

const deleteByIdAsync = async (req_id) => {
  console.info(`Deleting the author with req_id ${req_id} from database async...`);

  const bookings_requests = await queryAsync(
    "DELETE FROM bookings_requests WHERE req_id = $1 RETURNING *",
    [req_id]
  );
  return bookings_requests[0];
};

module.exports = {
  addAsync,
  getAllAsync,
  getByIdAsync,
  updateStatusByIdAsync,
  deleteByIdAsync,
  getUserByNameAndEmailAsync,
  getDoctorByNameAsync
};
