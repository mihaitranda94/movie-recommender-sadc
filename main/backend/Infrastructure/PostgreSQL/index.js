const { Pool } = require('pg');
const { getSecret } = require('docker-secret');

const options = {
  host: "localhost",
  database: "db",
  port: "5432",
  user: "postgres",
  password: "postgres",
};

const pool = new Pool(options);

const queryAsync = async (text, params) => {
  const start = Date.now();

  const {
    rows,
  } = await pool.query(text, params);
  const duration = Date.now() - start;
  console.log(`Query took ${duration} and returned ${rows.length} rows.`);

  return rows;
};

module.exports = {
  queryAsync,
};
