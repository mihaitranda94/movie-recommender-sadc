const ServerError = require('./ServerError.js');

class UserBody {
    constructor (body) {
        this.u_id = body.u_id;
        this.first_name = body.first_name;
        this.last_name = body.last_name;
        this.type = body.type;
        this.email = body.email;
        this.password = body.password;
        this.phone = body.phone;

        if (this.first_name == null || this.first_name < 1) {
            throw new ServerError("First name is missing", 400);
        }
    
        if (this.last_name == null || this.last_name.length < 1) {
            throw new ServerError("Last name is missing", 400);
        }

        if (this.type < 0 || this.type > 3) {
            throw new ServerError("Wrong type!", 404);
        }

        if (!this.email.includes("@") || !this.email.includes(".") ){
            throw new ServerError("email format is wrong!", 404);
        }
    }

    get First_name () {
        return this.first_name;
    }

    get Last_name () {
        return this.last_name;
    }


    get Type () {
        return this.type;
    }

    get Email () {
        return this.email;
    }

    get Password () {
        return this.password;
    }

    get Phone () {
        return this.phone;
    }

    

}


class UserLoginBody {
    constructor (body) {
        this.u_id = body.u_id;
        this.email = body.email;
        this.password = body.password;

        
    }

    get Email () {
        return this.email;
    }

    get Password () {
        return this.password;
    }
}

class UserRegisterResponse {
    constructor(user) {
        this.username = user.username;
        this.id = user.id;
        this.type = user.type;
    }
}
class UserLoginResponse {
    constructor(token, type) {
        this.type = type;
        this.token = token;
    }
}

module.exports =  {
    UserBody,
    UserLoginResponse,
    UserRegisterResponse,
    UserLoginBody
}

