const express = require('express');

const DoctorsRepository = require('../../Infrastructure/PostgreSQL/Repository/DoctorsRepository.js');
const ServerError = require('../Models/ServerError.js');
const AuthorizationFilter = require('../Filters/AuthorizationFilter.js');

const ResponseFilter = require('../Filters/ResponseFilter.js');

const Router = express.Router();

Router.post('/', AuthorizationFilter.authorizeRoles(1,2), async (req, res) => {
    
    

    const doctor = await DoctorsRepository.addAsync(req.body.name, req.body.dep_id, req.body.loc_id);

    ResponseFilter.setResponseDetails(res, 201, doctor, req.originalUrl);
});

Router.get('/', AuthorizationFilter.authorizeRoles(1,2,3), async (req, res) => {

    const Doctors = await DoctorsRepository.getAllAsync();

    ResponseFilter.setResponseDetails(res, 200, Doctors);
});

Router.get('/:id', AuthorizationFilter.authorizeRoles(1,2,3), async (req, res) => {
    const dep_id = req.params.id;
    const Doctors = await DoctorsRepository.getAllByDepAndLocAsync(dep_id);

    ResponseFilter.setResponseDetails(res, 200, Doctors);
});

Router.get('/doctor/:id', AuthorizationFilter.authorizeRoles(1,2,3), async (req, res) => {
    const d_id = req.params.id;
    const Doctors = await DoctorsRepository.getByIdAsync(d_id);

    ResponseFilter.setResponseDetails(res, 200, Doctors);
});

Router.get('/location/:id', AuthorizationFilter.authorizeRoles(1,2,3), async (req, res) => {
    const loc = await DoctorsRepository.getLocationByDoctorId(req.params.id);

    ResponseFilter.setResponseDetails(res, 200, loc);
});

Router.get('/department/:id', AuthorizationFilter.authorizeRoles(1,2,3), async (req, res) => {
    const dpt = await DoctorsRepository.getDepartmentByDoctorId(req.params.id);

    ResponseFilter.setResponseDetails(res, 200, dpt);
});


Router.put('/:d_id', AuthorizationFilter.authorizeRoles(2), async (req, res) => {
    
    const d_id = req.params.d_id;

    if (!d_id || d_id < 0) {
        throw new ServerError("Id should be a positive integer", 400);
    }

    const doctor = await DoctorsRepository.updateByIdAsync(d_id, req.body.name, req.body.dep_id, req.body.loc_id);

    if (!doctor) {
        throw new ServerError(`Doctor with id ${d_id} does not exist!`, 404);
    }

    ResponseFilter.setResponseDetails(res, 201, doctor, req.originalUrl);
});

// Router.put('/:id', AuthorizationFilter.authorizeRoles(1,2), async (req, res) => {

//     const authorBody = new AuthorPutBody(req.body, req.params.id);

//     const author = await DoctorsRepository.updateByIdAsync(authorBody.Id, authorBody.FirstName, authorBody.LastName);
        
//     if (!author) {
//         throw new ServerError(`Author with id ${id} does not exist!`, 404);
//     }

//     ResponseFilter.setResponseDetails(res, 200, new AuthorResponse(author));
// });

Router.delete('/:d_id', AuthorizationFilter.authorizeRoles(2, 4), async (req, res) => {
    const {
        d_id
    } = req.params;

    if (!d_id || d_id < 0) {
        throw new ServerError("Id should be a positive integer", 400);
    }
    
    const doctor = await DoctorsRepository.deleteByIdAsync(parseInt(d_id));
    
    if (!doctor) {
        throw new ServerError(`Doctor with id ${d_id} does not exist!`, 404);
    }

    ResponseFilter.setResponseDetails(res, 204, `Entity ${doctor.name} deleted succesfully`);
});

module.exports = Router;