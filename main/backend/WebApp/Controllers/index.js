const Router = require("express")();

const UsersController = require("./UsersController.js");
const DoctorsController = require("./DoctorsController");
const LocationController = require("./LocationController");
const DepartmentsController = require("./DepartmentsController");
const BookingsReqController = require("./BookingsReqController");
const BookingsController = require("./BookingsController");
const TestResultsController = require("./TestResultsController");
const MedsHistoryController = require("./MedsHistoryController");
const ReviewsController = require("./ReviewsController");




const authorizeAndExtractTokenAsync = require("../Filters/JWTFilter.js")
  .authorizeAndExtractTokenAsync;


Router.use("/users", UsersController);
Router.use("/doctors", authorizeAndExtractTokenAsync, DoctorsController);
Router.use("/location", authorizeAndExtractTokenAsync, LocationController);
Router.use("/departments", authorizeAndExtractTokenAsync, DepartmentsController);
Router.use("/bookings_requests", authorizeAndExtractTokenAsync, BookingsReqController);
Router.use("/test_results", authorizeAndExtractTokenAsync, TestResultsController);
Router.use("/meds_history", authorizeAndExtractTokenAsync, MedsHistoryController);
Router.use("/reviews", authorizeAndExtractTokenAsync, ReviewsController);


module.exports = Router;
