const express = require("express");

const MedsHistoryRepository = require("../../Infrastructure/PostgreSQL/Repository/MedsHistoryRepository.js");
const BookingsRepository = require("../../Infrastructure/PostgreSQL/Repository/BookingsRepository");
const ServerError = require("../Models/ServerError.js");

const ResponseFilter = require("../Filters/ResponseFilter.js");
const AuthorizationFilter = require("../Filters/AuthorizationFilter.js");

const Router = express.Router();

Router.post("/", AuthorizationFilter.authorizeRoles(2, 3), async (req, res) => {
  const { user_id, doc_name, data } = req.body;
  console.log(doc_name);
  const meds_history = await MedsHistoryRepository.addAsync(
    user_id,
    doc_name,
    data
  );

  ResponseFilter.setResponseDetails(res, 201, meds_history, req.originalUrl);
});

Router.get(
  "/:id",
  AuthorizationFilter.authorizeRoles(1, 2, 3),
  async (req, res) => {
    const u_id = req.params.id; 
    const meds_history = await MedsHistoryRepository.getByUidAsync(u_id);
    console.log(meds_history);

    ResponseFilter.setResponseDetails(res, 200, meds_history);
  }
);

Router.post(
  "/user_id",
  AuthorizationFilter.authorizeRoles(1, 2, 3),
  async (req, res) => {
    console.log(req.query);
    // const [first_name, last_name, email] = req.query;
    const meds_history = await MedsHistoryRepository.getUserByNameAndEmailAsync(
      req.body.first_name,
      req.body.last_name,
      req.body.email
    );

    ResponseFilter.setResponseDetails(res, 200, meds_history);
  }
);

Router.post(
  "/doc_id",
  AuthorizationFilter.authorizeRoles(1, 2, 3),
  async (req, res) => {
    const { name, dep_id, loc_id } = req.body;
    const meds_history = await MedsHistoryRepository.getDoctorByNameAsync(
      name,
      dep_id,
      loc_id
    );

    ResponseFilter.setResponseDetails(res, 200, meds_history);
  }
);

Router.put("/", AuthorizationFilter.authorizeRoles(2, 4), async (req, res) => {
  const { req_id, user_id, description, date, slot, doc_id, status } = req.body;
  const date_obj = new Date(date);

  console.log(req.body);
  console.log(date);
  const meds_history = await MedsHistoryRepository.updateStatusByIdAsync(
    req_id,
    user_id,
    description,
    date_obj,
    slot,
    doc_id,
    status
  );

  // try {
  //   if(meds_history){
  //     const bookings = await BookingsRepository.addAsync(
  //       user_id,
  //       description,
  //       date_obj,
  //       slot,
  //       doc_id,
  //       req_id
  //     );
  //   }

  // } catch (error) {
  //   throw new ServerError(error);
  // }

  // ResponseFilter.setResponseDetails(res, 201, meds_history, req.originalUrl);
});

// Router.get('/:id', AuthorizationFilter.authorizeRoles(RoleConstants.ADMIN, RoleConstants.MANAGER, RoleConstants.USER), async (req, res) => {
//     let {
//         id
//     } = req.params;

//     id = parseInt(id);

//     if (!id || id < 1) {
//         throw new ServerError("Id should be a positive integer", 400);
//     }

//     const Publisher = await MedsHistoryRepository.getByIdAsync(id);

//     if (!Publisher) {
//         throw new ServerError(`Publisher with id ${id} does not exist!`, 404);
//     }

//     ResponseFilter.setResponseDetails(res, 200, Publisher);
// });

// Router.put('/:id', AuthorizationFilter.authorizeRoles(RoleConstants.ADMIN, RoleConstants.MANAGER), async (req, res) => {

//     const PublisherBody = new PublisherPutBody(req.body, req.params.id);

//     const Publisher = await MedsHistoryRepository.updateByIdAsync(PublisherBody.Id, PublisherBody.Name, PublisherBody.AuthorID);

//     if (!Publisher) {
//         throw new ServerError(`Publisher with id ${id} does not exist!`, 404);
//     }

//     ResponseFilter.setResponseDetails(res, 200, Publisher);
// });

Router.delete(
  "/:req_id",
  AuthorizationFilter.authorizeRoles(1, 2, 3),
  async (req, res) => {
    const { req_id } = req.params;

    if (!req_id || req_id < 0) {
      throw new ServerError("Id should be a positive integer", 400);
    }

    const meds_history = await MedsHistoryRepository.deleteByIdAsync(
      parseInt(req_id)
    );

    if (!meds_history) {
      throw new ServerError(
        `meds_history with id ${req_id} does not exist!`,
        404
      );
    }

    ResponseFilter.setResponseDetails(
      res,
      204,
      `Entity ${meds_history.req_id} deleted succesfully`
    );
  }
);

module.exports = Router;
