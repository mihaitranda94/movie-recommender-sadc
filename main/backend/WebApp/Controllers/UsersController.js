const express = require('express');

const UsersManager = require('../../WebCore/Managers/UsersManager.js');
const UsersRepository = require('../../Infrastructure/PostgreSQL/Repository/UsersRepository.js');

const {
    UserBody,
    UserRegisterResponse,
    UserLoginResponse,
    UserLoginBody
} = require ('../Models/User.js');
const ResponseFilter = require('../Filters/ResponseFilter.js');
const AuthorizationFilter = require('../Filters/AuthorizationFilter.js');
const RoleConstants = require('../Constants/Roles.js');
const { authorizeAndExtractTokenAsync } = require('../Filters/JWTFilter.js');

const Router = express.Router();

Router.post('/register', async (req, res) => {

    const userBody = new UserBody(req.body);
    const user = await UsersManager.registerAsync(userBody.First_name, userBody.Last_name, userBody.Password, userBody.Type,
        userBody.Email, userBody.Phone );

    ResponseFilter.setResponseDetails(res, 201, user);
});

Router.post('/login', async (req, res) => {

    const userBody = new UserLoginBody(req.body);
    const userDto = await UsersManager.authenticateAsync(userBody.Email, userBody.Password);

    ResponseFilter.setResponseDetails(res, 200, userDto);
});

Router.get('/', async (req, res) => {

    const users = await UsersRepository.getAllAsync();

    ResponseFilter.setResponseDetails(res, 200, users);
});


Router.get('/:id', async (req, res) => {
    const u_id = req.params.id
    const users = await UsersRepository.getByIdAsync(u_id);

    ResponseFilter.setResponseDetails(res, 200, users);
});

Router.post('/emailandphone', async (req, res) => {
    const [email, phone] = req.body
    const users = await UsersRepository.getUserByEmailAndPhoneAsync(email, phone);

    ResponseFilter.setResponseDetails(res, 200, users);
});

Router.put('/:userId/role/:roleId', AuthorizationFilter.authorizeRoles(1), async (req, res) => {
    let {
        userId,
        roleId
    } = req.params;

    userId = parseInt(userId);
    roleId = parseInt(roleId);

    /**
     * TODO
     */
});

module.exports = Router;