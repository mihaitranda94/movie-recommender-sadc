const express = require('express');

const ReviewsRepository = require('../../Infrastructure/PostgreSQL/Repository/ReviewsRepository');
const ServerError = require('../Models/ServerError.js');

const ResponseFilter = require('../Filters/ResponseFilter.js');
const AuthorizationFilter = require('../Filters/AuthorizationFilter.js');
const { request } = require('express');

const Router = express.Router();

Router.post('/', AuthorizationFilter.authorizeRoles(1), async (req, res) => {
    
    const {user_id, doc_id, rating, message} = req.body;
    console.log(req.body);
    const reviews = await ReviewsRepository.addAsync(user_id, doc_id, rating, message);

    ResponseFilter.setResponseDetails(res, 201, reviews, req.originalUrl);
});

Router.get('/:id', AuthorizationFilter.authorizeRoles(1,2,3,4), async (req, res) => {
    const id = req.params.id;

    const reviews = await ReviewsRepository.getAllAsync(id);

    ResponseFilter.setResponseDetails(res, 200, reviews);
});




// Router.post('/user_id', AuthorizationFilter.authorizeRoles(1,2,3), async (req, res) => {
//     console.log(req.query);
//     console.log('\n\n\n');
//     // const [first_name, last_name, email] = req.query;
//     const reviews = await ReviewsRepository.getUserByNameAndEmailAsync(
//         req.body.first_name, 
//         req.body.last_name, 
//         req.body.email );

//     ResponseFilter.setResponseDetails(res, 200, reviews);
// });


// Router.post('/doc_id', AuthorizationFilter.authorizeRoles(1,2,3), async (req, res) => {
//     const {name} = req.body;
//     const reviews = await ReviewsRepository.getDoctorByNameAsync(name);

//     ResponseFilter.setResponseDetails(res, 200, reviews);
// });

// Router.get('/:id', AuthorizationFilter.authorizeRoles(RoleConstants.ADMIN, RoleConstants.MANAGER, RoleConstants.USER), async (req, res) => {
//     let {
//         id
//     } = req.params;

//     id = parseInt(id);

//     if (!id || id < 1) {
//         throw new ServerError("Id should be a positive integer", 400);
//     }
       
//     const Publisher = await ReviewsRepository.getByIdAsync(id);
    
//     if (!Publisher) {
//         throw new ServerError(`Publisher with id ${id} does not exist!`, 404);
//     }

//     ResponseFilter.setResponseDetails(res, 200, Publisher);
// });


// Router.put('/:id', AuthorizationFilter.authorizeRoles(RoleConstants.ADMIN, RoleConstants.MANAGER), async (req, res) => {

//     const PublisherBody = new PublisherPutBody(req.body, req.params.id);

//     const Publisher = await ReviewsRepository.updateByIdAsync(PublisherBody.Id, PublisherBody.Name, PublisherBody.AuthorID);
        
//     if (!Publisher) {
//         throw new ServerError(`Publisher with id ${id} does not exist!`, 404);
//     }

//     ResponseFilter.setResponseDetails(res, 200, Publisher);
// });

Router.delete('/:r_id', AuthorizationFilter.authorizeRoles(1,2,4), async (req, res) => {
    const {
        r_id
    } = req.params;

    if (!r_id || r_id < 0) {
        throw new ServerError("Id should be a positive integer", 400);
    }
    
    const reviews = await ReviewsRepository.deleteByIdAsync(parseInt(r_id));
    
    if (!reviews) {
        throw new ServerError(`reviews with id ${r_id} does not exist!`, 404);
    }

    ResponseFilter.setResponseDetails(res, 204, `Entity ${reviews.r_id} deleted succesfully`);
});


module.exports = Router;