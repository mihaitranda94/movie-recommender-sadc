const express = require('express');

const BookingsRepository = require('../../Infrastructure/PostgreSQL/Repository/BookingsRepository');
const ServerError = require('../Models/ServerError.js');

const ResponseFilter = require('../Filters/ResponseFilter.js');
const AuthorizationFilter = require('../Filters/AuthorizationFilter.js');
const { request } = require('express');

const Router = express.Router();

Router.post('/', AuthorizationFilter.authorizeRoles(2,4), async (req, res) => {
    
    const {user_id, description, date, slot, doc_id, request_id} = req.body;
    console.log(req.body);
    const bookings = await BookingsRepository.addAsync(user_id, description, date, slot, doc_id, request_id);

    ResponseFilter.setResponseDetails(res, 201, bookings, req.originalUrl);
});

Router.get('/', AuthorizationFilter.authorizeRoles(1,2,4), async (req, res) => {

    const bookings = await BookingsRepository.getAllAsync();

    ResponseFilter.setResponseDetails(res, 200, bookings);
});




// Router.post('/user_id', AuthorizationFilter.authorizeRoles(1,2,3), async (req, res) => {
//     console.log(req.query);
//     console.log('\n\n\n');
//     // const [first_name, last_name, email] = req.query;
//     const bookings = await BookingsRepository.getUserByNameAndEmailAsync(
//         req.body.first_name, 
//         req.body.last_name, 
//         req.body.email );

//     ResponseFilter.setResponseDetails(res, 200, bookings);
// });


// Router.post('/doc_id', AuthorizationFilter.authorizeRoles(1,2,3), async (req, res) => {
//     const {name} = req.body;
//     const bookings = await BookingsRepository.getDoctorByNameAsync(name);

//     ResponseFilter.setResponseDetails(res, 200, bookings);
// });

// Router.get('/:id', AuthorizationFilter.authorizeRoles(RoleConstants.ADMIN, RoleConstants.MANAGER, RoleConstants.USER), async (req, res) => {
//     let {
//         id
//     } = req.params;

//     id = parseInt(id);

//     if (!id || id < 1) {
//         throw new ServerError("Id should be a positive integer", 400);
//     }
       
//     const Publisher = await BookingsRepository.getByIdAsync(id);
    
//     if (!Publisher) {
//         throw new ServerError(`Publisher with id ${id} does not exist!`, 404);
//     }

//     ResponseFilter.setResponseDetails(res, 200, Publisher);
// });


// Router.put('/:id', AuthorizationFilter.authorizeRoles(RoleConstants.ADMIN, RoleConstants.MANAGER), async (req, res) => {

//     const PublisherBody = new PublisherPutBody(req.body, req.params.id);

//     const Publisher = await BookingsRepository.updateByIdAsync(PublisherBody.Id, PublisherBody.Name, PublisherBody.AuthorID);
        
//     if (!Publisher) {
//         throw new ServerError(`Publisher with id ${id} does not exist!`, 404);
//     }

//     ResponseFilter.setResponseDetails(res, 200, Publisher);
// });

Router.delete('/:b_id', AuthorizationFilter.authorizeRoles(2,4), async (req, res) => {
    const {
        b_id
    } = req.params;

    if (!b_id || b_id < 0) {
        throw new ServerError("Id should be a positive integer", 400);
    }
    
    const bookings = await BookingsRepository.deleteByIdAsync(parseInt(b_id));
    
    if (!bookings) {
        throw new ServerError(`bookings with id ${b_id} does not exist!`, 404);
    }

    ResponseFilter.setResponseDetails(res, 204, `Entity ${bookings.b_id} deleted succesfully`);
});


module.exports = Router;