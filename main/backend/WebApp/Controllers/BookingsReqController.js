const express = require("express");

const BookingsReqRepository = require("../../Infrastructure/PostgreSQL/Repository/BookingsReqRepository");
const BookingsRepository = require("../../Infrastructure/PostgreSQL/Repository/BookingsRepository");
const ServerError = require("../Models/ServerError.js");

const ResponseFilter = require("../Filters/ResponseFilter.js");
const AuthorizationFilter = require("../Filters/AuthorizationFilter.js");

const Router = express.Router();

Router.post(
  "/",
  AuthorizationFilter.authorizeRoles(1, 2, 3),
  async (req, res) => {
    const { user_id, description, date, slot, doc_id, status } = req.body;
    const d = new Date(date.toString());
    console.log(date);
    
    const bookings_req = await BookingsReqRepository.addAsync(
      user_id,
      description,
      d,
      slot,
      doc_id,
      status
    );

    ResponseFilter.setResponseDetails(res, 201, bookings_req, req.originalUrl);
  }
);

Router.get(
  "/",
  AuthorizationFilter.authorizeRoles(1, 2, 3),
  async (req, res) => {
    const bookings_req = await BookingsReqRepository.getAllAsync();

    ResponseFilter.setResponseDetails(res, 200, bookings_req);
  }
);

Router.post(
  "/user_id",
  AuthorizationFilter.authorizeRoles(1, 2, 3),
  async (req, res) => {
    console.log(req.query);
    console.log("\n\n\n");
    // const [first_name, last_name, email] = req.query;
    const bookings_req = await BookingsReqRepository.getUserByNameAndEmailAsync(
      req.body.first_name,
      req.body.last_name,
      req.body.email
    );

    ResponseFilter.setResponseDetails(res, 200, bookings_req);
  }
);

Router.post(
  "/doc_id",
  AuthorizationFilter.authorizeRoles(1, 2, 3),
  async (req, res) => {
    const { name, dep_id, loc_id } = req.body;
    const bookings_req = await BookingsReqRepository.getDoctorByNameAsync(name, dep_id, loc_id);

    ResponseFilter.setResponseDetails(res, 200, bookings_req);
  }
);

Router.put("/", AuthorizationFilter.authorizeRoles(2, 4), async (req, res) => {
  const { req_id, user_id, description, date, slot, doc_id, status } = req.body;
    const date_obj = new Date(date);
  

  console.log(req.body);
  console.log(date);
  const bookings_req = await BookingsReqRepository.updateStatusByIdAsync(req_id, user_id, description, date_obj, slot, doc_id, status);

  // try {
  //   if(bookings_req){
  //     const bookings = await BookingsRepository.addAsync(
  //       user_id,
  //       description,
  //       date_obj,
  //       slot,
  //       doc_id,
  //       req_id
  //     );
  //   }
    
  // } catch (error) {
  //   throw new ServerError(error);
  // }

  // ResponseFilter.setResponseDetails(res, 201, bookings_req, req.originalUrl);
});

// Router.get('/:id', AuthorizationFilter.authorizeRoles(RoleConstants.ADMIN, RoleConstants.MANAGER, RoleConstants.USER), async (req, res) => {
//     let {
//         id
//     } = req.params;

//     id = parseInt(id);

//     if (!id || id < 1) {
//         throw new ServerError("Id should be a positive integer", 400);
//     }

//     const Publisher = await BookingsReqRepository.getByIdAsync(id);

//     if (!Publisher) {
//         throw new ServerError(`Publisher with id ${id} does not exist!`, 404);
//     }

//     ResponseFilter.setResponseDetails(res, 200, Publisher);
// });

// Router.put('/:id', AuthorizationFilter.authorizeRoles(RoleConstants.ADMIN, RoleConstants.MANAGER), async (req, res) => {

//     const PublisherBody = new PublisherPutBody(req.body, req.params.id);

//     const Publisher = await BookingsReqRepository.updateByIdAsync(PublisherBody.Id, PublisherBody.Name, PublisherBody.AuthorID);

//     if (!Publisher) {
//         throw new ServerError(`Publisher with id ${id} does not exist!`, 404);
//     }

//     ResponseFilter.setResponseDetails(res, 200, Publisher);
// });

Router.delete(
  "/:req_id",
  AuthorizationFilter.authorizeRoles(1, 2, 3),
  async (req, res) => {
    const { req_id } = req.params;

    if (!req_id || req_id < 0) {
      throw new ServerError("Id should be a positive integer", 400);
    }

    const bookings_req = await BookingsReqRepository.deleteByIdAsync(
      parseInt(req_id)
    );

    if (!bookings_req) {
      throw new ServerError(
        `bookings_req with id ${req_id} does not exist!`,
        404
      );
    }

    ResponseFilter.setResponseDetails(
      res,
      204,
      `Entity ${bookings_req.req_id} deleted succesfully`
    );
  }
);

module.exports = Router;
