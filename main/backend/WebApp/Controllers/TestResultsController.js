const express = require("express");

const TestResultsRepository = require("../../Infrastructure/PostgreSQL/Repository/TestResultsRepository.js");
const BookingsRepository = require("../../Infrastructure/PostgreSQL/Repository/BookingsRepository");
const ServerError = require("../Models/ServerError.js");

const ResponseFilter = require("../Filters/ResponseFilter.js");
const AuthorizationFilter = require("../Filters/AuthorizationFilter.js");

const Router = express.Router();

Router.post("/", AuthorizationFilter.authorizeRoles(2, 3), async (req, res) => {
  const { user_id, doc_name, data } = req.body;
  console.log(doc_name);
  const test_results = await TestResultsRepository.addAsync(
    user_id,
    doc_name,
    data
  );

  ResponseFilter.setResponseDetails(res, 201, test_results, req.originalUrl);
});

Router.get(
  "/:id",
  AuthorizationFilter.authorizeRoles(1, 2, 3),
  async (req, res) => {
    const u_id = req.params.id; 
    const test_results = await TestResultsRepository.getByUidAsync(u_id);
    console.log(test_results);

    ResponseFilter.setResponseDetails(res, 200, test_results);
  }
);

Router.post(
  "/user_id",
  AuthorizationFilter.authorizeRoles(1, 2, 3),
  async (req, res) => {
    console.log(req.query);
    // const [first_name, last_name, email] = req.query;
    const test_results = await TestResultsRepository.getUserByNameAndEmailAsync(
      req.body.first_name,
      req.body.last_name,
      req.body.email
    );

    ResponseFilter.setResponseDetails(res, 200, test_results);
  }
);

Router.post(
  "/doc_id",
  AuthorizationFilter.authorizeRoles(1, 2, 3),
  async (req, res) => {
    const { name, dep_id, loc_id } = req.body;
    const test_results = await TestResultsRepository.getDoctorByNameAsync(
      name,
      dep_id,
      loc_id
    );

    ResponseFilter.setResponseDetails(res, 200, test_results);
  }
);

Router.put("/", AuthorizationFilter.authorizeRoles(2, 4), async (req, res) => {
  const { req_id, user_id, description, date, slot, doc_id, status } = req.body;
  const date_obj = new Date(date);

  console.log(req.body);
  console.log(date);
  const test_results = await TestResultsRepository.updateStatusByIdAsync(
    req_id,
    user_id,
    description,
    date_obj,
    slot,
    doc_id,
    status
  );

  // try {
  //   if(test_results){
  //     const bookings = await BookingsRepository.addAsync(
  //       user_id,
  //       description,
  //       date_obj,
  //       slot,
  //       doc_id,
  //       req_id
  //     );
  //   }

  // } catch (error) {
  //   throw new ServerError(error);
  // }

  // ResponseFilter.setResponseDetails(res, 201, test_results, req.originalUrl);
});

// Router.get('/:id', AuthorizationFilter.authorizeRoles(RoleConstants.ADMIN, RoleConstants.MANAGER, RoleConstants.USER), async (req, res) => {
//     let {
//         id
//     } = req.params;

//     id = parseInt(id);

//     if (!id || id < 1) {
//         throw new ServerError("Id should be a positive integer", 400);
//     }

//     const Publisher = await TestResultsRepository.getByIdAsync(id);

//     if (!Publisher) {
//         throw new ServerError(`Publisher with id ${id} does not exist!`, 404);
//     }

//     ResponseFilter.setResponseDetails(res, 200, Publisher);
// });

// Router.put('/:id', AuthorizationFilter.authorizeRoles(RoleConstants.ADMIN, RoleConstants.MANAGER), async (req, res) => {

//     const PublisherBody = new PublisherPutBody(req.body, req.params.id);

//     const Publisher = await TestResultsRepository.updateByIdAsync(PublisherBody.Id, PublisherBody.Name, PublisherBody.AuthorID);

//     if (!Publisher) {
//         throw new ServerError(`Publisher with id ${id} does not exist!`, 404);
//     }

//     ResponseFilter.setResponseDetails(res, 200, Publisher);
// });

Router.delete(
  "/:req_id",
  AuthorizationFilter.authorizeRoles(1, 2, 3),
  async (req, res) => {
    const { req_id } = req.params;

    if (!req_id || req_id < 0) {
      throw new ServerError("Id should be a positive integer", 400);
    }

    const test_results = await TestResultsRepository.deleteByIdAsync(
      parseInt(req_id)
    );

    if (!test_results) {
      throw new ServerError(
        `test_results with id ${req_id} does not exist!`,
        404
      );
    }

    ResponseFilter.setResponseDetails(
      res,
      204,
      `Entity ${test_results.req_id} deleted succesfully`
    );
  }
);

module.exports = Router;
