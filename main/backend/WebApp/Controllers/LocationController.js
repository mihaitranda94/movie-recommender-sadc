const express = require('express');

const LocationRepository = require('../../Infrastructure/PostgreSQL/Repository/LocationRepository');
const ServerError = require('../Models/ServerError.js');

const ResponseFilter = require('../Filters/ResponseFilter.js');
const AuthorizationFilter = require('../Filters/AuthorizationFilter.js');

const Router = express.Router();

Router.post('/', AuthorizationFilter.authorizeRoles(1,2), async (req, res) => {
    
    
    console.log(req.body);
    const location = await LocationRepository.addAsync(req.body.name, req.body.address);

    ResponseFilter.setResponseDetails(res, 201, location, req.originalUrl);
});

Router.get('/', AuthorizationFilter.authorizeRoles(1,2,3), async (req, res) => {

    const location = await LocationRepository.getAllAsync();

    ResponseFilter.setResponseDetails(res, 200, location);
});


Router.put('/:l_id', AuthorizationFilter.authorizeRoles(2), async (req, res) => {
    
    const l_id = req.params.l_id;

    if (!l_id || l_id < 0) {
        throw new ServerError("Id should be a positive integer", 400);
    }

    const location = await LocationRepository.updateByIdAsync(l_id, req.body.name, req.body.address);

    if (!location) {
        throw new ServerError(`location with id ${l_id} does not exist!`, 404);
    }

    ResponseFilter.setResponseDetails(res, 201, location, req.originalUrl);
});

// Router.get('/:id', AuthorizationFilter.authorizeRoles(RoleConstants.ADMIN, RoleConstants.MANAGER, RoleConstants.USER), async (req, res) => {
//     let {
//         id
//     } = req.params;

//     id = parseInt(id);

//     if (!id || id < 1) {
//         throw new ServerError("Id should be a positive integer", 400);
//     }
       
//     const Publisher = await LocationRepository.getByIdAsync(id);
    
//     if (!Publisher) {
//         throw new ServerError(`Publisher with id ${id} does not exist!`, 404);
//     }

//     ResponseFilter.setResponseDetails(res, 200, Publisher);
// });


// Router.put('/:id', AuthorizationFilter.authorizeRoles(RoleConstants.ADMIN, RoleConstants.MANAGER), async (req, res) => {

//     const PublisherBody = new PublisherPutBody(req.body, req.params.id);

//     const Publisher = await LocationRepository.updateByIdAsync(PublisherBody.Id, PublisherBody.Name, PublisherBody.AuthorID);
        
//     if (!Publisher) {
//         throw new ServerError(`Publisher with id ${id} does not exist!`, 404);
//     }

//     ResponseFilter.setResponseDetails(res, 200, Publisher);
// });

Router.delete('/:l_id', AuthorizationFilter.authorizeRoles(2,4), async (req, res) => {
    const {
        l_id
    } = req.params;
    console.log(l_id);
    if (!l_id || l_id < 0) {
        throw new ServerError("Id should be a positive integer", 400);
    }
    
    const location = await LocationRepository.deleteByIdAsync(parseInt(l_id));
    
    if (!location) {
        throw new ServerError(`location with id ${l_id} does not exist!`, 404);
    }

    ResponseFilter.setResponseDetails(res, 204, `Entity ${location.name} deleted succesfully`);
});


module.exports = Router;