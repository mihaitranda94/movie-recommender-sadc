const express = require('express');

const DepartmentsRepository = require('../../Infrastructure/PostgreSQL/Repository/DepartmentsRepository');
const ServerError = require('../Models/ServerError.js');

const ResponseFilter = require('../Filters/ResponseFilter.js');
const AuthorizationFilter = require('../Filters/AuthorizationFilter.js');
const { query } = require('express');

const Router = express.Router();

Router.post('/', AuthorizationFilter.authorizeRoles(1,2), async (req, res) => {
    
    
    console.log(req.body);
    const department = await DepartmentsRepository.addAsync(req.body.name, req.body.loc_id, req.body.description);

    ResponseFilter.setResponseDetails(res, 201, department, req.originalUrl);
});

Router.get('/', AuthorizationFilter.authorizeRoles(1,2,3), async (req, res) => {

    const department = await DepartmentsRepository.getAllAsync();

    ResponseFilter.setResponseDetails(res, 200, department);
});


Router.get('/:id', AuthorizationFilter.authorizeRoles(1,2,3), async (req, res) => {
    const loc_id = req.params.id;
    console.log(req.params);
    console.log(loc_id);
    const department = await DepartmentsRepository.getAllByLocIdAsync(loc_id);

    ResponseFilter.setResponseDetails(res, 200, department);
});

// Router.get('/:id', AuthorizationFilter.authorizeRoles(RoleConstants.ADMIN, RoleConstants.MANAGER, RoleConstants.USER), async (req, res) => {
//     let {
//         id
//     } = req.params;

//     id = parseInt(id);

//     if (!id || id < 1) {
//         throw new ServerError("Id should be a positive integer", 400);
//     }
       
//     const Publisher = await DepartmentsRepository.getByIdAsync(id);
    
//     if (!Publisher) {
//         throw new ServerError(`Publisher with id ${id} does not exist!`, 404);
//     }

//     ResponseFilter.setResponseDetails(res, 200, Publisher);
// });


// Router.put('/:id', AuthorizationFilter.authorizeRoles(RoleConstants.ADMIN, RoleConstants.MANAGER), async (req, res) => {

//     const PublisherBody = new PublisherPutBody(req.body, req.params.id);

//     const Publisher = await DepartmentsRepository.updateByIdAsync(PublisherBody.Id, PublisherBody.Name, PublisherBody.AuthorID);
        
//     if (!Publisher) {
//         throw new ServerError(`Publisher with id ${id} does not exist!`, 404);
//     }

//     ResponseFilter.setResponseDetails(res, 200, Publisher);
// });

Router.delete('/:dpt_id', AuthorizationFilter.authorizeRoles(1,2), async (req, res) => {
    const {
        dpt_id
    } = req.params;

    if (!dpt_id || dpt_id < 0) {
        throw new ServerError("Id should be a positive integer", 400);
    }
    
    const department = await departmentsRepository.deleteByIdAsync(parseInt(dpt_id));
    
    if (!department) {
        throw new ServerError(`department with id ${dpt_id} does not exist!`, 404);
    }

    ResponseFilter.setResponseDetails(res, 204, `Entity ${department.name} deleted succesfully`);
});


module.exports = Router;