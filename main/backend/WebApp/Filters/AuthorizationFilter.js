const ServerError = require('../Models/ServerError.js');

const authorizeRoles = (...roles) => {
    return (req, res, next) => {
        try {
            for (let i = 0; i < roles.length; i++) {
                // TODO: in functie de implemetare
                // verificati daca req.user contine role sau userRole
                if (req.user.type === roles[i]) { // observati cum in req exista obiectul user trimis la middlewareul anterior, de autorizare token
                        return next();
                }
            }
            throw new ServerError("Nu esti autorizat (n-ai rolul necesar)", 403);
        } catch (error) {
            throw new ServerError(error, 404);
        }
        
        
    }
};
 
module.exports = {
    authorizeRoles
}