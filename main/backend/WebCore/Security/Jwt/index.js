const jwt = require('jsonwebtoken');

const ServerError = require('../../../WebApp/Models/ServerError.js');

const options = {
    issuer: "admin",
    subject: "medicine",
    audience: "patients and doctors"
};
const secretKey  = "myawesomeultrasecretkey";

const generateTokenAsync = async (payload) => {
    // TODO
    // HINT: folositi functia "sign" din biblioteca jsonwebtoken
    // HINT2: seamana cu functia verify folosita mai jos ;)
    /*
     payload este JwtPayloadDto
    */
    try{
        const token = await jwt.sign(payload, secretKey, options);
        return token;
    } catch (err){
        throw new ServerError(err, 401);
   }
    

};

const verifyAndDecodeDataAsync = async (token) => {
    try {
        const decoded = await jwt.verify(token, secretKey, options);
        return decoded;
    } catch (err) {
        console.trace(err);
        throw new ServerError("Eroare la decriptarea tokenului!", 401);
    }
};

module.exports = {
    generateTokenAsync,
    verifyAndDecodeDataAsync
};