class AuthenticatedUserDto {
    constructor (token, id, type) {
        this.id = id;
        this.token = token;
        this.type = type;
    }

    get Token() {
        return this.token; 
    }

    get Type() {  
        return this.type;
    } 

    get Id() {
        return this.id;
    }
}

module.exports = AuthenticatedUserDto;