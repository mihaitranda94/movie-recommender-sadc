const UsersRepository = require('../../Infrastructure/PostgreSQL/Repository/UsersRepository.js');
const AuthenticatedUserDto = require('../DTOs/AuthenticatedUserDto.js');
const RegisteredUserDto = require('../DTOs/RegisteredUserDto.js');
const JwtPayloadDto = require('../DTOs/JwtPayloadDto.js');

const { hashPassword, comparePlainTextToHashedPassword } = require('../Security/Password')
const { generateTokenAsync } = require('../Security/Jwt');
const ServerError = require('../../WebApp/Models/ServerError.js');
const { token } = require('morgan');

const authenticateAsync = async (email, plainTextPassword) => {

    console.info(`Authenticates user with email ${email}`);

    const user = await UsersRepository.getByEmailWithTypeAsync(email);
    if (!user) {
        throw new ServerError(`Utilizatorul cu email ${email} nu exista in sistem!`, 404);
    }

    /**
     * TODO
     * 
     * pas 1: verifica daca parola este buna (hint: functia compare)
     * pas 1.1.: compare returneaza true sau false. Daca parola nu e buna, arunca eroare
     * pas 2: genereaza token cu payload-ul JwtPayload
     * pas 3: returneaza AuthenticatedUserDto
     */

    const isLegit = comparePlainTextToHashedPassword(plainTextPassword, user.password);
    if (!isLegit){
        throw new ServerError("Bad password!", 404);
    }
    const payload = {
        id: user.id,
        type: user.type,
    };
    const gen_token = await generateTokenAsync(payload);
    const return_obj = {
        token: gen_token,
        id: user.u_id,
        email: user.email,
        type: user.type
    }
    return return_obj;
     

};

const registerAsync = async (first_name, last_name, plainTextPassword, type, email, phone) => {
    /**
     * TODO
     * 
     * pas 1: cripteaza parola
     * pas 2: adauga (email, parola criptata) in baza de date folosind UsersRepository.addAsync
     * pas 3: returneaza RegisteredUserDto
     * 
     */
    try {
        const pass_hashed = await hashPassword(plainTextPassword);
        console.info(`${first_name}, ${last_name}, ${pass_hashed},  ${type}, ${email}, ${phone}`);
        const user_db = await UsersRepository.addAsync(first_name, last_name, plainTextPassword, type, email, phone);
        
        return user_db; 
    } catch (error) {
        throw new ServerError(error, 404);
    }
    

    
};

module.exports = {
    authenticateAsync,
    registerAsync
}