const express = require('express')
const app = express();
const cors =require("cors");
const pool = require("../db");
//middleware
const {spawn} = require('child_process');

app.use(express.json());
app.use(cors());
//ROUTES

//REGISTER AND LOGIN 
app.use("/auth", require("./routes/jwtAuth"));

app.post('/ceva/:email', async (req, res) => {
  const email = req.params.email;
  console.log(email);
    const user = await pool.query('SELECT * FROM users_informations WHERE email = $1', [
      email,
    ]);
    console.log(user.rows[0].favourite_movies);
    if (user.rows.length == 0) {
      return res.status(401).json("No User like that");
    }


  var dataToSend;
  // spawn new child process to call the python script
  const python = spawn('python', ['similarity.py',user.rows[0].favourite_movies[0]]);
  // collect data from script
  python.stdout.on('data', function (data) {
   console.log('Pipe data from python script ...');
   dataToSend = data.toString();
  });
  // in close event we are sure that stream from child process is closed
  python.on('close', (code) => {
  console.log(`child process close all stdio with code ${code}`);
  // send data to browser
  res.send(dataToSend)
 
  });
  
 });

app.listen(5000, () => {
    console.log(`Server is starting on port 5000`);
  });
  