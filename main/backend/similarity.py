from scipy.spatial import distance
import pandas as pd
import itertools
import sys 

def turn_to_list2(str):
    str = str.strip('[]')
    str = str.replace(' ','')
    str = str.replace("'",'')
    str = str.split(',')
    test_list = [int(i) for i in str]
    return test_list

def similarity(movieId1, movieId2):

    a = movies.iloc[movieId1]
    b = movies.iloc[movieId2]
    
    genresA = turn_to_list2(a['genres_bin'])
    genresB = turn_to_list2(b['genres_bin'])
    # print(genresA)
    # print(genresB)
    # print(a['original_title'])
    # print(b['original_title'])
    # print(type(genresA))
    
    genreDistance = distance.cosine(genresA, genresB)
    # print(genreDistance)
    if(pd.isna(genreDistance)) :
        genreDistance=1
    
    scoreA = turn_to_list2(a['cast_bin'])
    scoreB = turn_to_list2(b['cast_bin'])
    scoreDistance = distance.cosine(scoreA, scoreB)
    # print(scoreA)
    # print(scoreB)
    # print(scoreDistance)
    
    directA = turn_to_list2(a['directors_bin'])
    directB = turn_to_list2(b['directors_bin'])
    directDistance = distance.cosine(directA, directB)
    # print(directA)
    # print(directB)
    # print(directDistance)
    
    wordsA = turn_to_list2( a['words_bin'])
    wordsB =  turn_to_list2(b['words_bin'])
    wordsDistance = distance.cosine(wordsA, wordsB)
    # print(wordsDistance)
    # if(a['original_title'] == 'Men with Brooms' or a['original_title'] == 'Vampires Suck' or a['original_title'] == 'The Road' or a['original_title'] == 'Yes Man' or a['original_title'] == 'Wanderlust'  ):
    #     print(a['original_title'])
    #     print(genreDistance)
    #     print(scoreDistance)
    #     print(directDistance)
    #     print(wordsDistance)
    #     print(pd.isna(genreDistance))
    #     print('______________________')
    return genreDistance + directDistance + scoreDistance + wordsDistance

movies = pd.read_csv('encoded_dataset.csv')
# print(sys.argv[1])
# for i,j in zip(movies['directors'],movies.index):
#     if 'Michael Bay' in i:
#         print(j)
# print(similarity(0,114))
indexxx = 0
for i in (movies.index):
    a = movies.iloc[i]
    if (a['original_title'] ==sys.argv[1]):
        indexxx = i
# print(indexxx)
harryratings = []
dic = {}
for i in range(0,3200):
    a = movies.iloc[i]
    # print(a)
    dic[a['original_title']] = (similarity(i,indexxx))
# {k: v for k, v in sorted(dic.items(), key=lambda item: item[1])}
# dict(sorted(dic.items(), key=lambda item: item[1]))
sorted_dict = {}
sorted_keys = sorted(dic, key=dic.get)
# out = dict(itertools.islice(sorted_keys.items(), 10))
# print(sorted_keys.fetch(10))
i=0
resp = []
for value in sorted_keys:
    if(i<10):
        resp.append(value)
    i= i+1
print(resp)
