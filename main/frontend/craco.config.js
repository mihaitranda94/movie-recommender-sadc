const CracoLessPlugin = require("craco-antd");

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              "primary-color": "#3c9ae8",
              "link-color": "#177ddc",
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
