import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import {
  DesktopOutlined,
  PieChartOutlined,
  FileOutlined,
  TeamOutlined,
  UserOutlined,
  GlobalOutlined,
  EditOutlined,
} from "@ant-design/icons";
import AuthService from "./services/auth.service";
import { Layout, Menu, Button, notification, Select } from "antd";
import { HomeOutlined, CalendarOutlined } from "@ant-design/icons";
import  { Fragment, useState, useEffect } from "react";
// import Login from "./components/login.component";
import Register from "./components/register.component";
import Home from "./components/home.component";
import HomeNotAuth from "./components/homeNotAuth.component";
import Profile from "./components/profile.component";
import LoginForm from "./components/form.component";
import Recommentation from "./components/recommendations";
import backgroundHome from "./images/background.jpg";
import logo from "./images/film_logo.jpg";
import { Avatar, Affix } from "antd";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.less";

import ColdStart from "./components/coldStart.component";

const { SubMenu } = Menu;
const { Header, Content, Footer } = Layout;



export default class App extends Component {
  constructor(props) {
    super(props);
    
    this.logOut = this.logOut.bind(this);
    this.changeTheme = this.changeTheme.bind(this);

    this.state = {
      // showModeratorBoard: false,
      // showAdminBoard: false,
      currentUser: undefined,
      collapsed: false,
      from: "ro",
      to: "en",
      bottom: 10,
    };
  }
  
  
  componentDidMount() {
    const user = AuthService.getCurrentUser();
    console.log(user);

    if (user) {
      this.setState({
        currentUser: user,
      });
    }
  }

  changeTheme() {
    if (this.state.theme === "light") {
      this.setState({
        theme: "dark",
      });
    } else {
      this.setState({
        theme: "light",
      });
    }
  }

  onCollapse = (collapsed) => {
    console.log(collapsed);
    this.setState({ collapsed });
  };

  logOut() {
    AuthService.logout();
    localStorage.setItem("email", null);
  }

  

  render() {
    let success = true;
    const { currentUser, collapsed, from, to, bottom } = this.state;

    return (

      <Layout className="layout" style={{ minHeight: "100vh" }}>
        <Affix>
          <Header style={{ padding: "0px" }}>
            <a
              href="/"
              className="logo"
              style={{
                float: "left",
                width: "fit-content",
                height: "fit-content",
              }}
            >
              <img src={logo} style={{width:70, height:60}}></img> Movie recommender App
            </a>

            <Menu mode="horizontal" defaultSelectedKeys={["1"]}>
              <Menu.Item
                key="home"
                icon={
                  <HomeOutlined
                    style={{ verticalAlign: "middle", borderRadius: "10px" }}
                  />
                }
              >
                <Link to={"/"}>
                  Home
                </Link>
              </Menu.Item>
              {currentUser ? (
                <Menu.Item
                  key="user"
                  style={{ float: "right", marginLeft: "10px" }}
                >
                  <Avatar
                    shape="square"
                    size="large"
                    icon={<UserOutlined />}
                  />
                </Menu.Item>
              ) : null}
              
              {currentUser ? (
                <Menu.Item key="logout" style={{ float: "right" }}>
                  <a href={"/form"} onClick={this.logOut}>
                    Log Out
                  </a>
                </Menu.Item>
              ) : (
                <Menu.Item key="login" style={{ float: "right" }}>
                  <Link to={"/form"}>Login</Link>
                </Menu.Item>
              )}

              {currentUser ? null : (
                <Menu.Item key="register" style={{ float: "right" }}>
                  <Link to={"/Register"}>Register</Link>
                </Menu.Item>
              )}
            </Menu>
          </Header>
        </Affix>

        {/* User dashboard */}
        {currentUser ? (
          <Layout >
            <Affix offsetTop={67}>
            </Affix>

            <Layout style={{ padding: "0 24px 24px" }}>
              <Content
                className="site-layout-background"
                style={{
                  padding: 24,
                  margin: 0,
                  minHeight: 280,
                }}
              >
                <Switch style={{ float: "right" }}>
                  <Route
                    exact
                    path={["/", "/home"]}
                    component={() => (
                      <Home currentUser={currentUser}></Home>
                    )}
                  />
                  <Route
                    exact
                    path={["/", "/ceva"]}
                    component={() => (
                      <Recommentation currentUser={currentUser}></Recommentation>
                    )}
                  />
                  <Route exact path="/register" component={ColdStart} />
                  <Route exact path="/profile" component={Profile} />
                  <Route exact path="/form" component={LoginForm} />

                </Switch>
              </Content>

              <Footer style={{ textAlign: "center" }}>
                Movie Recommender App ©2021 Created by Cinefilii
                {/* <Button onClick={this.changeTheme}>
                  {this.state.theme}Mode
                </Button> */}
              </Footer>
            </Layout>
          </Layout>
        ) : (
          <Layout style={{ padding: "0 24px 24px" }}>
            <Content
              className="site-layout-background"
              style={{
                padding: 24,
                margin: 0,
                minHeight: 280,
              }}
            >
              <Switch style={{ float: "right" }}>
                <Route exact path={["/", "/home"]}>
                  <HomeNotAuth currentUser={currentUser} />
                </Route>
                <Route exact path="/register" component={ColdStart} />
                <Route exact path="/profile" component={Profile} />
                <Route exact path="/form" component={LoginForm} />
              </Switch>
            </Content>
            <Footer style={{ textAlign: "center" }}>
              Movie Recommender App ©2021 Created by Cinefilii
              {/* <Button onClick={this.changeTheme} style={{float:'right'}}>{this.state.theme}Mode</Button> */}
            </Footer>
          </Layout>
        )}


      </Layout>

    );
  }
  
}
