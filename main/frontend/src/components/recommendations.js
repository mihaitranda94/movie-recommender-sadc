import React, { Component, useEffect, useState } from "react";
import axios from "axios";
import {
  Carousel,
  Tabs,
  Progress,
  Card,
  Row,
  Col,
  Divider,
  notification,
  PageHeader,
} from "antd";
import requestService from "../services/request.service";
import { relativeTimeRounding } from "moment";
import { Layout, Menu, Button, Select } from "antd";
import { useHistory } from "react-router";
// const spawn = require("child_process").spawn;
const { TabPane } = Tabs;

function callback(key) {
  console.log(key);
}

// console.log(cast);
//   }
//   const resp = await axios.get("https://api.themoviedb.org/3/search/movie?api_key=68ce5d35cd89de62e1fc171bbcaa753a&query=" + movie);

//   localStorage.setItem('moviek', dataToSend)
export default function Recommendations({ currentUser, props }) {
  const [movies, setMovies] = useState(undefined);
  const [similar, setSimilar] = useState(undefined);
  const [movieYouWatched, setMovieYouWatched] = useState(undefined);

  const getK = async () => {
    const email = localStorage.getItem("email")
    console.log("fac req");
    const trending_resp = await axios.post("http://localhost:5000/ceva/" +email);
    console.log("heihei");
    console.log(trending_resp.data)
    console.log("intra in get movies");

    var array = trending_resp.data.split(",");
    console.log(array)
    var full_movies = [];
    console.log('aici intra?')
    try {
        
      for (let index = 2; index < array.length; index++) {
        console.log(array[index]);
        if(( array[index].includes('\\'))){

        } else {
          const resp = await axios.get(
            "https://api.themoviedb.org/3/search/movie?api_key=68ce5d35cd89de62e1fc171bbcaa753a&query=" +
              array[index]
          );
          if (resp.data.results.length < 1) {
            notification.error({ message: "no movies found" });
          }
          // console.log(resp.data.results[0])
          full_movies.push(resp.data.results[0]);
          // console.log(resp);
        }

      }
    } catch (err) {
      console.log(err);
    }
    setMovies(full_movies);
  };

  const getOtherUsers = async () => {
      var full_movies = []
    const email = localStorage.getItem("email")
    console.log("fac req pt similar users");
    const trending_resp = await axios.post("http://localhost:5000/auth/users/" + email);
    console.log(trending_resp.data)
    var array = trending_resp.data
    setMovieYouWatched("Similar Users who watched " + array[array.length - 1] + " also watched these:")
    try {
        
        for (let index = 0; index < array.length-1; index++) {
          console.log(array[index]);
          const resp = await axios.get(
            "https://api.themoviedb.org/3/search/movie?api_key=68ce5d35cd89de62e1fc171bbcaa753a&query=" +
              array[index]
          );
          if (resp.data.results.length < 1) {
            notification.error({ message: "no movies found" });
          }
          // console.log(resp.data.results[0])
          full_movies.push(resp.data.results[0]);
          // console.log(resp);
        }
      } catch (err) {
        console.log(err);
      }
      setSimilar(full_movies);
  }

//   const getMovies = async () => {
//     console.log("intra in movies");
//     try {
//       for (let index = 0; index < movies.length; index++) {
//         console.log(movies[index]);
//         const resp = await axios.get(
//           "https://api.themoviedb.org/3/search/movie?api_key=68ce5d35cd89de62e1fc171bbcaa753a&query=" +
//             movies[index]
//         );
//         console.log(resp);
//       }
//     } catch (err) {
//       console.log(err);
//     }
//   };

  useEffect(() => {
    // print();
    getK();
    getOtherUsers();
    // console.log(localStorage.getItem("moviek"))
  }, []);

//   useEffect(() => {
//     // print();
//     if (movies != undefined) {
//       getMovies();
//     }
//     // console.log(localStorage.getItem("moviek"))
//   }, [movies]);

  return (
    <div>
      <PageHeader title="Trending Movies" subTitle="Last week" />
      {movies != undefined ?
      (<Carousel dotPosition="top" effect="fade" vertical="true" autoplay>
      {movies.map((value, index) => {
        const posterURL = "https://image.tmdb.org/t/p/original";
        return (
        <div>
          <h3 style={{
            height: '660px',
            color: 'black',
            textAlign: 'bottom',
            lineHeight: '160px',
            textAlign: 'center',
            background: '#364d79',
            backgroundImage: `url(${posterURL + value.backdrop_path})`,
            backgroundPosition: 'center',
            textShadow: '-1px -1px 3px rgba(0.90, 0.90, 0.90, 0.75)',
            borderColor: 'black',
            borderRadius: '10px',
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover'

          }}>
            <a href="" style={{color: 'whitesmoke'}}>
              {value.title}
            </a>
          </h3>
        </div>
        )
      }
      )}
    </Carousel >
      
      ) : null}
      <PageHeader title={movieYouWatched} subTitle="Last week" />
      {similar != undefined ?
      (<Carousel dotPosition="top" effect="fade" vertical="true" autoplay>
        {similar.map((value, index) => {
          const posterURL = "https://image.tmdb.org/t/p/original";
          return (
          <div>
            <h3 style={{
              height: '660px',
              color: 'black',
              textAlign: 'bottom',
              lineHeight: '160px',
              textAlign: 'center',
              background: '#364d79',
              backgroundImage: `url(${posterURL + value.backdrop_path})`,
              backgroundPosition: 'center',
              textShadow: '-1px -1px 3px rgba(0.90, 0.90, 0.90, 0.75)',
              borderColor: 'black',
              borderRadius: '10px',
              backgroundRepeat: 'no-repeat',
              backgroundSize: 'cover'

            }}>
              <a href="" style={{color: 'whitesmoke'}}>
                {value.title}
              </a>
            </h3>
          </div>
          )
        }
        )}
      </Carousel> 
      
      ) : null}
    </div>
  );
}
