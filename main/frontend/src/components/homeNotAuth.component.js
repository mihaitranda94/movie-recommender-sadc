import React, { Component } from "react";
import {Redirect, useHistory} from 'react-router-dom';
import { Empty, Alert } from 'antd';

const steps = [
  {
    title: 'First',
    content: 'First-content',
  },
  {
    title: 'Second',
    content: 'Second-content',
  },
  {
    title: 'Last',
    content: 'Last-content',
  },
];

const HomeNotAuth = () => {
  const [current, setCurrent] = React.useState(0);
  const history = useHistory();
  const next = () => {
    setCurrent(current + 1);
  };

  const prev = () => {
    setCurrent(current - 1);
  };

  const onClose = () => {
    history.push('/form');
    window.location.reload();
  };

  return (
    <>

        <Alert
      message="You are not Signed In!"
      description="Close the alert and it will take you to the sign in page."
      type="warning"
      closable
      onClose={onClose}
      style={{marginBottom: '100px'}}
    />
      <Empty></Empty>
    </>
  );
};

export default HomeNotAuth;