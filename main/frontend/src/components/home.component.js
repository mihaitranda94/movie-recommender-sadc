import React, { Component, useEffect, useState } from "react";
import axios from "axios";
import { Carousel, Tabs, Progress, Card, Row, Col, Divider, notification, PageHeader } from "antd";
import requestService from "../services/request.service";
import { relativeTimeRounding } from "moment";
import { Layout, Menu, Button,  Select } from "antd";
import { useHistory } from 'react-router';

const { TabPane } = Tabs;


function callback(key) {
  console.log(key);
}

export default function Home({ currentUser, props }) {

  const [pending, setPending] = useState(0);
  const [confirmed, setConfirmed] = useState(0);
  const [rejected, setRejected] = useState(0);
  const [trending, setTrending] = useState([]);
  const [cast, setCast] = useState([]);
  const [movies, setMovies] = useState([]);
  const [moviesKeywords, setMoviesKeywords] = useState([]);
  const [moviesIds, setMoviesIds] = useState([]);
  const [moviesCast, setMoviesCast] = useState([]);
  const history = useHistory();

  const contentStyle = {
    height: '160px',
    color: '#fff',
    lineHeight: '160px',
    textAlign: 'center',
    background: '#364d79',
  };
  //https://api.themoviedb.org/3/search/person?api_key=68ce5d35cd89de62e1fc171bbcaa753a&language=en-US&query=julia+roberts&page=1

  const getTrending = async () => {
    const trending_resp = await axios.get("https://api.themoviedb.org/3/trending/movie/week?api_key=68ce5d35cd89de62e1fc171bbcaa753a")
    if (trending_resp.data.results.length < 1) {
      notification.error({ message: "no movies found" })
    } else {
      setTrending(trending_resp.data.results);
    }
    console.log(trending_resp.data.results)
  }
  const getMoviesByKeywordsMovies = async () => {
    var moviesAltered = []
    var movieIds = []
    var keywords = []
    var full_movies = []
    for (let i = 0; i <  movies.length; i++) {
      var mov = movies[i];
      mov = mov.replace(/ /g, "+");
      moviesAltered.push(mov);
    }
    for (let i = 0; i <  moviesAltered.length; i++) {
      var movie = moviesAltered[i];
      const resp = await axios.get("https://api.themoviedb.org/3/search/movie?api_key=68ce5d35cd89de62e1fc171bbcaa753a&query=" + movie);
      if (resp.data.results.length < 1) {
        notification.error({ message: "no movies found" })
      } else {
      //  full_movies.push(resp.data.results[0]);
        // setTrending(trending_resp.data.results);
        // console.log(resp.data.results[0].id);
        const value = resp.data.results[0].id;
        movieIds.push(resp.data.results[0].id);
        const resp2 = await axios.get("https://api.themoviedb.org/3/movie/" + value + "/keywords?api_key=68ce5d35cd89de62e1fc171bbcaa753a");
        if (resp2.data.keywords.length < 1) {
          notification.error({ message: "no movies found" })
        } else {
          // console.log(resp2.data.keywords[0].name)
          var val = resp2.data.keywords[0].name;
          val = val.replace(" ", '+');
          console.log(val);
          const resp3= await axios.get("https://api.themoviedb.org/3/search/keyword?api_key=68ce5d35cd89de62e1fc171bbcaa753a&query=" + val);
          console.log(resp3.data.results[0].id);
          keywords.push(resp3.data.results[0].id);
        }
      }
      
      // console.log(resp.data.results[0])

    }
    for (let i = 0; i <  keywords.length; i++) {
      const reFinal = await axios.get("https://api.themoviedb.org/3/keyword/" + keywords[i]+ "/movies?api_key=68ce5d35cd89de62e1fc171bbcaa753a&language=en-US&include_adult=false");
        if (reFinal.data.results.length < 1) {
          notification.error({ message: "no movies found" })
        } else {
          for (let j = 1; j < 4; j++) {
            full_movies.push((reFinal.data.results[j]));
            }
        }
    }
    console.log(full_movies);
    console.log(movieIds);
    setMoviesIds(movieIds);
    setMoviesKeywords(full_movies);
  }
  //https://api.themoviedb.org/3/search/movie?api_key=###&query=
  const getMoviesByCast = async () => {
    var full_resp = []
    var full_movies = []
    for (let i = 0; i <  cast.length; i++)
    {  
      var actor = cast[i];
      actor = actor.replace(" ", '+');
      // console.log(actor);

      const trending_resp = await axios.get("https://api.themoviedb.org/3/search/person?api_key=68ce5d35cd89de62e1fc171bbcaa753a&language=en-US&query=" + actor)
      if (trending_resp.data.results.length < 1) {
        notification.error({ message: "no movies found" })
      } else {
        var arr = [];
        var value;

        for (let i = 0; i <  trending_resp.data.results[0].known_for.length; i++) {
          if (trending_resp.data.results[0].known_for[i].original_title != undefined) {
            var value = trending_resp.data.results[0].known_for[i].original_title;
            value =  value.replace(/ /g, "+");
            arr.push(value);
            // console.log(trending_resp.data.results[0].known_for[i].original_title)
          }

          if (i>4) {
            i=trending_resp.data.results[0].known_for.length;
          } 
        }
        if(i== 0) {
          full_resp= arr;
        } else {
          full_resp = full_resp.concat(arr);
        }
        
     
        // console.log(new_arr);
      }
      if(i>10) {
        i= cast.length;
      }
    }
    // setMoviesCast(full_resp);
    // console.log(full_resp);
    for (let i = 0; i <  full_resp.length; i++) {
      var movie = full_resp[i];
      const resp = await axios.get("https://api.themoviedb.org/3/search/movie?api_key=68ce5d35cd89de62e1fc171bbcaa753a&query=" + movie);
      if (resp.data.results.length < 1) {
        notification.error({ message: "no movies found" })
      } else {
       full_movies.push(resp.data.results[0]);
        // setTrending(trending_resp.data.results);
      }
      // console.log(resp.data.results[0])

    }
    console.log(full_movies);
    setMoviesCast(full_movies);
 
  }
  
  const getCast = async () => {
    const email = localStorage.getItem("email");
    const trending_resp = await axios.post("http://localhost:5000/auth/cast/" + email)
    if (!trending_resp) {
      notification.error({ message: "no cast found" })
    } else {
      setCast(trending_resp.data)
    }
    console.log(trending_resp.data);
    // console.log(cast);
  }

  const getMovies = async () => {
    const email = localStorage.getItem("email");
    const trending_resp = await axios.post("http://localhost:5000/auth/movies/" + email)
    if (!trending_resp) {
      notification.error({ message: "no movies found" })
    } else {
      setMovies(trending_resp.data)
    }
    console.log(trending_resp.data)

  }

  const redir = async () => {
    history.push("/ceva");  }

  useEffect(() => {
    getTrending();
    getCast();
    getMovies();
    // getMoviesByCast();
    // getMoviesByKeywordsMovies();
  
    // console.log(localStorage.getItem("email"));
    // console.log(moviesCast);
  }, [])
  useEffect(() => {
    getMoviesByCast();
    getMoviesByKeywordsMovies();
  }, [cast])

  useEffect(() => {
    getMoviesByKeywordsMovies();
  }, [movies])

  return (
    <div>
      <PageHeader
        title="Trending Movies"
        subTitle="Last week"
      />
      <Carousel dotPosition="top" effect="fade" vertical="true" autoplay>
        {trending.map((value, index) => {
          const posterURL = "https://image.tmdb.org/t/p/original";
          return (
          <div>
            <h3 style={{
              height: '660px',
              color: 'black',
              textAlign: 'bottom',
              lineHeight: '160px',
              textAlign: 'center',
              background: '#364d79',
              backgroundImage: `url(${posterURL + value.backdrop_path})`,
              backgroundPosition: 'center',
              textShadow: '-1px -1px 3px rgba(0.90, 0.90, 0.90, 0.75)',
              borderColor: 'black',
              borderRadius: '10px',
              backgroundRepeat: 'no-repeat',
              backgroundSize: 'cover'

            }}>
              <a href="" style={{color: 'whitesmoke'}}>
                {value.title}
              </a>
            </h3>
          </div>
          )
        }
        )}
      </Carousel >
      <PageHeader
        title="Movies based on your preferred cast"
        subTitle="Last week"
      />
      <Carousel dotPosition="top" effect="fade" vertical="true" autoplay>
        {moviesCast.map((value, index) => {
          const posterURL = "https://image.tmdb.org/t/p/original";
          return (
          <div>
            <h3 style={{
              height: '660px',
              color: 'black',
              textAlign: 'bottom',
              lineHeight: '160px',
              textAlign: 'center',
              background: '#364d79',
              backgroundImage: `url(${posterURL + value.backdrop_path})`,
              backgroundPosition: 'center',
              textShadow: '-1px -1px 3px rgba(0.90, 0.90, 0.90, 0.75)',
              borderColor: 'black',
              borderRadius: '10px',
              backgroundRepeat: 'no-repeat',
              backgroundSize: 'cover'

            }}>
              <a href="" style={{color: 'whitesmoke'}}>
                {value.title}
              </a>
            </h3>
          </div>
          )
        }
        )}
      </Carousel >
      <PageHeader
        title="Movies based on your watched"
        subTitle="Last week"
      />
      <Carousel dotPosition="top" effect="fade" vertical="true" autoplay>
        {moviesKeywords.map((value, index) => {
          const posterURL = "https://image.tmdb.org/t/p/original";
          return (
          <div>
            <h3 style={{
              height: '660px',
              color: 'black',
              textAlign: 'bottom',
              lineHeight: '160px',
              textAlign: 'center',
              background: '#364d79',
              backgroundImage: `url(${posterURL + value.backdrop_path})`,
              backgroundPosition: 'center',
              textShadow: '-1px -1px 3px rgba(0.90, 0.90, 0.90, 0.75)',
              borderColor: 'black',
              borderRadius: '10px',
              backgroundRepeat: 'no-repeat',
              backgroundSize: 'cover'

            }}>
              <a href="" style={{color: 'whitesmoke'}}>
                {value.title}
              </a>
            </h3>
          </div>
          )
        }
        )}
      </Carousel >
      <Divider></Divider>
      {
        currentUser.type === 2 ? (
          <>
            <Card title="Users Appointment Requests Statistics">
              <Row>
                <Col xs={{ span: 5, offset: 1 }} lg={{ span: 6, offset: 2 }}>
                  <h4>Pending</h4>
                  <Progress type="circle" percent={pending} strokeColor={"yellow"} />
                </Col>
                <Col xs={{ span: 11, offset: 1 }} lg={{ span: 6, offset: 2 }}>
                  <h4>Confirmed</h4>
                  <Progress type="circle" percent={confirmed} strokeColor={"green"} />
                </Col>
                <Col xs={{ span: 5, offset: 1 }} lg={{ span: 6, offset: 2 }}>
                  <h4>Rejected</h4>
                  <Progress type="circle" percent={rejected} strokeColor={"red"} />
                </Col>
              </Row>
            </Card>
            <Divider></Divider>
          </>
        ) : (
          <>
            <h3></h3>
            <Button  onClick={() => redir()}>Try more personalised recommendations:</Button>
          </>

        )
      }

    </div >
  );
}
