import React, { useEffect, useState } from "react";
import axios from "axios";
import CountryDropdown from "country-dropdown-with-flags-for-react";
import {
  Form,
  Input,
  Button,
  Radio,
  Select,
  Cascader,
  DatePicker,
  InputNumber,
  TreeSelect,
  Switch,
  Divider,
  notification,
  Space,
  Card,
  Affix,
  Statistic,
} from "antd";

const { Search } = Input;

export default function ColdStart(props) {
  const [componentSize, setComponentSize] = useState("default");
  const [firstName, setFirstName] = useState(undefined);
  const [lastName, setLastName] = useState(undefined);
  const [email, setEmail] = useState(undefined);
  const [metroSwitch, setMetroSwitch] = useState(false);
  const [busSwitch, setBusSwitch] = useState(false);
  const [tickets, setTickets] = useState([]);
  const [selectValue, setSelectValue] = useState(0);
  const [adultTickets, setAdultTickets] = useState(0);
  const [studentTickets, setStudentTickets] = useState(0);
  const [childrenTickets, setChildrenTickets] = useState(0);
  const [age, setAge] = useState(undefined);
  const [policy, setPolicy] = useState(false);
  const [submit, setSubmit] = useState(undefined);
  const [password, setPassword] = useState(undefined);
  const [country, setCountry] = useState(undefined);
  const [movies, setMovies] = useState([]);
  const [movieSelect, setMovieSelect] = useState([]);
  const [movieQuery, setMovieQuery] = useState(undefined);
  const [cast, setCast] = useState([]);
  const [castSelect, setCastSelect] = useState([]);
  const [castQuery, setCastQuery] = useState(undefined);
  const [directors, setDirectors] = useState([]);
  const [directorSelect, setDirectorSelect] = useState([]);
  const [directorQuery, setDirectorQuery] = useState(undefined);
  const [movieFav, setMovieFav] = useState(["nothing"]);

  const { Option } = Select;
  const container = React.useRef(null);

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const validateMessages = {
    required: "${label} is required!",
    types: {
      email: "${label} is not a valid email!",
      number: "${label} is not a valid number!",
    },
    number: {
      range: "${label} must be between ${min} and ${max}",
    },
  };

  const handleCountry = (country) => {
    console.log(country);
    setCountry(country);
  };

  const handleAgeChange = (value) => {
    setAge(value);
  };

  const handleSubmit = async () => {
    console.log(firstName.target.value + lastName.target.value);
    console.log(email.target.value);
    console.log(age);
    console.log(movieSelect)
    console.log(castSelect)
    // console.log(busSwitch);
    // console.log(parseInt(adultTickets + studentTickets + childrenTickets));
    const body = {
      first_name: firstName.target.value.toString(),
      last_name: lastName.target.value.toString(),
      email: email.target.value.toString(),
      password: password.target.value.toString(),
      age: age,
      country: country.target.value.toString(),
      favourite_movies: movieSelect,
      favourite_cast: castSelect,
    };
    const data = [
      { first_name: firstName.target.value.toString() },
      { last_name: lastName.target.value.toString() },
      { email: email.target.value.toString() },
      { age: age.toString() },
      { country: country.target.value.toString() },
      { favourite_movies: movieSelect },
      { favourite_cast: castSelect },
    ];
    // const daba = [{ foo: "foo" }, { bar: "bar" }];
    // console.log(daba);
    console.log(data);

    let fields = props.fields ? props.fields : [];
    try {
      const response = await axios.post(
        "http://localhost:5000/auth/register",
        body
      );
      if(response) {
        localStorage.setItem("email",  email.target.value.toString());
        notification.success({ message: "Registered in successfully" });
        props.history.push("/home");

      }
    } catch (e) {
      console.error("not a good register");
    }
  };

  const searchMovies = async () => {
    const movie_resp = await axios.get(
      "https://api.themoviedb.org/3/search/movie?api_key=68ce5d35cd89de62e1fc171bbcaa753a&query=" +
        movieQuery
    );
    if (movie_resp.data.results.length < 1) {
      notification.error({ message: "no movie found" });
    } else {
      console.log(movie_resp.data.results[0].original_title);
      const vari = movieFav;
      vari.push(movie_resp.data.results[0].original_title);
      setMovieFav(vari);
      console.log("movies array   !!" + vari);
      console.log(movieQuery);
      setMovieSelect(
        movie_resp.data.results.map((value, index) => {
          return value
            ? {
                option: <Option key={index}>{value.title}</Option>,
                element: value,
              }
            : null;
        })
      );
    }
  };

  const searchCast = async () => {
    const cast_resp = await axios.get(
      "https://api.themoviedb.org/3/search/person?api_key=68ce5d35cd89de62e1fc171bbcaa753a&query=" +
        castQuery
    );
    if (cast_resp.data.results.length < 1) {
      notification.error({ message: "no cast found" });
    } else {
      setCastSelect(
        cast_resp.data.results.map((value, index) => {
          return value
            ? {
                option: <Option key={index}>{value.name}</Option>,
                element: value,
              }
            : null;
        })
      );
    }
  };

  const updateMovies = () => {
    var value;
    var arr = [];
    for (value in movies) {
      arr.append(value.element.original_title);
    }
    setMovieSelect(arr);
  };

  useEffect(() => {
    if (movieQuery) searchMovies();
  }, [movieQuery]);

  useEffect(() => {
    if (castQuery) searchCast(castQuery);
  }, [castQuery]);

  useEffect(() => {
    if (submit) {
      handleSubmit();
      setSubmit(false);
    }
  }, [submit]);

  return (
    <>
      <Form
        labelCol={{
          span: 4,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
        initialValues={{
          size: componentSize,
        }}
        onValuesChange={onFormLayoutChange}
        size={componentSize}
        validateMessages={validateMessages}
      >
        <Form.Item label="Form Size" name="size">
          <Radio.Group>
            <Radio.Button value="small">Small</Radio.Button>
            <Radio.Button value="default">Default</Radio.Button>
            <Radio.Button value="large">Large</Radio.Button>
          </Radio.Group>
        </Form.Item>
        <Divider />
        <Form.Item
          label="First Name"
          name={["user", "firstName"]}
          rules={[{ required: true }]}
        >
          <Input
            onChange={(value) => {
              setFirstName(value);
            }}
          />
        </Form.Item>
        <Form.Item
          label="Last Name"
          name={["user", "lastName"]}
          rules={[{ required: true }]}
        >
          <Input
            onChange={(value) => {
              setLastName(value);
            }}
          />
        </Form.Item>
        <Form.Item
          label="Email"
          name={["user", "email"]}
          rules={[{ type: "email", required: true }]}
        >
          <Input
            onChange={(value) => {
              setEmail(value);
            }}
            type="email"
          ></Input>
        </Form.Item>
        <Form.Item
          label="Password"
          // name={["user", "email"]}
          rules={[{ type: "password", required: true }]}
        >
          <Input
            onChange={(value) => {
              setPassword(value);
            }}
            type="password"
          ></Input>
        </Form.Item>
        <Form.Item
          label="Age"
          name={["user", "tickets"]}
          rules={[{ type: "number", min: 1, max: 99, required: true }]}
        >
          <InputNumber onChange={(value) => setAge(value)} />
        </Form.Item>
        <Form.Item
          label="Country"
          name={["user", "country"]}
          rules={[{ required: true }]}
        >
          <CountryDropdown
            preferredCountries={["ro", "us"]}
            value=""
            handleChange={handleCountry}
          ></CountryDropdown>
        </Form.Item>

        <Divider />
        <Form.Item
          label="Search for favourite movies"
          name={["user", "movies"]}
        >
          <Select
            showSearch
            placeholder="Choose movies you watched and enjoyed..."
            value={movieQuery}
            showArrow={true}
            filterOption={false}
            onSearch={(value) => {
              setMovieQuery(value);
            }}
            onSelect={(value) => {
              console.info(value + " thisss");
              setMovies([...movies, movieSelect[value]]);
              setMovieQuery(null);
              setMovieSelect([]);
              console.info("movies: " + movies);
            }}
            notFoundContent={null}
          >
            {movieSelect.map((val, index) => val.option)}
          </Select>
        </Form.Item>

        <Form.Item
          label="Search for favourite cast and directors"
          name={["user", "cast"]}
        >
          <Select
            showSearch
            placeholder="Choose cast and directors you like..."
            value={castQuery}
            showArrow={true}
            filterOption={false}
            onSearch={(value) => {
              setCastQuery(value);
            }}
            onSelect={(value) => {
              setCast([...cast, castSelect[value]]);
              setCastQuery(null);
              setCastSelect([]);
              console.info("cast: " + cast);
            }}
            notFoundContent={null}
          >
            {castSelect.map((val, index) => val.option)}
          </Select>
        </Form.Item>

        <Form.Item
          name={["user", "selected"]}
          rules={[{ type: "number", min: 1, max: 99 }]}
        >
          <Statistic
            title="Selected Movies"
            value={movies.map((value, index) => {
              return (
                "(" +
                value.element.original_title +
                "; " +
                value.element.release_date +
                ")"
              );
            })}
            style={{
              marginRight: 32,
              marginLeft: 240,
              marginTop: 10,
              marginBottom: 10,
            }}
          />
          <Button
            type="primary"
            style={{
              marginRight: 32,
              marginLeft: 240,
              marginTop: 10,
              marginBottom: 10,
            }}
            onClick={() => setMovies([])}
          >
            {" "}
            Clear movies
          </Button>
          <Statistic
            title="Selected Cast"
            value={cast.map((value, index) => {
              return value.element.name;
            })}
            style={{
              marginRight: 32,
              marginLeft: 240,
              marginTop: 10,
              marginBottom: 10,
            }}
          />
          <Button
            type="primary"
            style={{
              marginRight: 32,
              marginLeft: 240,
              marginTop: 10,
              marginBottom: 10,
            }}
            onClick={() => setCast([])}
          >
            {" "}
            Clear People
          </Button>
        </Form.Item>
        <Form.Item label="Submit Data">
          <Button
            onClick={() => {
              var arr = [];
              for (let i=0 ; i< movies.length; i++) {
                arr.push(movies[i].element.original_title);
              }
              setMovieSelect(arr);
              var arr2 = []
              for (let i=0 ; i< cast.length; i++) {
                arr2.push(cast[i].element.name);
              }
              setCastSelect(arr2);
              setSubmit(true);
              // updateMovies();
              // console.log(movies[0].element.original_title)
            
              
            }}
          >
            Confirm
          </Button>
        </Form.Item>
      </Form>
    </>
  );
}
