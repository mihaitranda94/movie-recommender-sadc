import React, { useEffect, useState } from "react";
import "antd/dist/antd.css";
import "../index.css";
import axios from "axios";
import {
  Form,
  Input,
  Button,
  Checkbox,
  Alert,
  Row,
  notification,
  Space,
  Card,
} from "antd";
import { UserOutlined, LockOutlined, TrophyFilled } from "@ant-design/icons";
import AuthService from "../services/auth.service";
import { password } from "pg/lib/defaults";
import { Redirect } from "react-router";
import { toast } from "react-toastify";

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 8 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 8 },
};

export default function LoginForm(props) {
  const [email, setEmail] = React.useState(undefined);
  const [password, setPassword] = React.useState(undefined);
  const [success, setSuccess] = React.useState(undefined);
  const [submitFlag, setSubmitFlag] = React.useState(false);

  function onChangePassword(e) {
    setPassword(e.target.value);
    console.error("parolaaa " + e.target.value);
  }

  function onChangeEmail(e) {
    setEmail(e.target.value);
    console.error("emailll " + e.target.value);
  }

  // functin onFinishFailed() {
  //   this.setState({
  //     loginFailed: true,
  //   });
  // }

  const handleLogin = async (e) => {
    console.log(password);
    console.log(email);
    const body = { email, password };
    try {
      const response = await axios.post(
        "http://localhost:5000/auth/login",
        body
      );
      if (response.data.jwtToken) {
        localStorage.setItem("jwt_token", response.data.jwtToken);
        notification.success({ message: "Logged in successfully" });
        console.log("succes");
        localStorage.setItem('user', {email, password});
        props.history.push("/home");
        localStorage.setItem("email",  email);
      } else {
        notification.error({ message: "Logged in failed" });
      }
    } catch (error) {
      console.log(error);
      throw new Error(error);
    }
  };

  useEffect(() => {
    if (submitFlag) {
      handleLogin();
    }
  }, [submitFlag]);

  return (
    <Space direction="vertical" style={{ width: "100%" }}>
      <Card>
        <Form
          {...layout}
          name="normal_login"
          className="login-form"
          initialValues={{
            remember: true,
          }}
          onFinish={() => {
            setSubmitFlag(true);
          }}
          onFinishFailed={() => {
            notification.error({ message: "Logged in failed" });
          }}
          // ref={(c) => {
          //   this.form = c;
          // }}
        >
          {/* {this.handleLogin == true ? <Redirect to="/home3" /> : null}
          {this.state.success ? <Redirect to="/home3" /> : null} */}

          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: "Please input your Email address!",
              },
            ]}
            style={{ display: "flex", justifyContent: "center" }}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Email"
              type="text"
              className="form-control"
              name="email"
              value={email}
              onChange={(e) => {
                onChangeEmail(e);
              }}
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Please input your Password!",
              },
            ]}
            style={{ display: "flex", justifyContent: "center" }}
          >
            <Input
              autosize="true"
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Password"
              name="password"
              value={password}
              onChange={(e) => {
                onChangePassword(e);
              }}
            />
          </Form.Item>
          <Form.Item style={{ display: "flex", justifyContent: "center" }}>
            <Form.Item name="remember" valuePropName="checked" noStyle>
              <Checkbox>Remember me</Checkbox>
            </Form.Item>

            <a className="login-form-forgot" href="">
              Forgot password
            </a>
          </Form.Item>

          <Form.Item style={{ display: "flex", justifyContent: "center" }}>
            <Button
              style={{ margin: "1%" }}
              type="primary"
              htmlType="submit"
              className="login-form-button"
              onClick
            >
              Log in
            </Button>
            Or <a href="/register">Register now!</a>
          </Form.Item>
        </Form>
      </Card>
    </Space>
  );
}

//ReactDOM.render(<NormalLoginForm />, document.getElementById('container'));
