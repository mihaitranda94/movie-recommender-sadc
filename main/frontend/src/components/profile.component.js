import React, { Component } from "react";
import AuthService from "../services/auth.service";

export default class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentUser: AuthService.getCurrentUser()
    };
    console.log(this.state.currentUser);
  }

  render() {
    const { currentUser } = this.state;
    let type = undefined;
    if( currentUser.type === 1){
      type = 'Patient';
    } else if(currentUser.type === 2){
      type = 'Admin';
    } else if(currentUser.type === 3){
      type = 'Doctor';
    } else {
      type = 'Support';
    }
    console.log(this.state.currentUser);
    return (
      <div className="container">
        <header className="jumbotron">
          <h3>
            <strong>{currentUser.email}</strong> Profile
          </h3>
        </header>
        <p>
          <strong>Token:</strong>{" "}
          {currentUser.token.substring(0, 20)} ...{" "}
          {currentUser.token.substr(currentUser.token.length - 20)}
        </p>
        
        <p>
          <strong>Email:</strong>{" "}
          {currentUser.email}
        </p>
        <strong>Authorities:</strong>{" "}
        
          {type}
        
      </div>
    );
  }
}