import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "http://localhost:5000/api/bookings";

class BookingService {
  async getAllBookings() {
    return await axios.get(API_URL + "/", { headers: authHeader() });
  }

  

  async postBooking(body){
    return await axios.post(API_URL + "/", body, {headers: authHeader() });
  }
}

export default new BookingService();
