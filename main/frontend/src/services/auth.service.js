import axios from "axios";

const API_URL = "http://localhost:5000/api/users/";

class AuthService {
  login = async (email, password) => {
    try {
      // const response_login = await axios.post(API_URL + "login", {
      //   email,
      //   password,
      // });
      // console.log(response_login.data.response);
      // if (response_login.data.response.token) {
        const body = {email, password};
        localStorage.setItem("user", JSON.stringify(body));
        return body;
      //}
    } catch (error) {
      throw new Error("Email or Password Incorrect");
    }
    
      
  };

  getUserByEmailandPhone = async (body) => {
    return await axios.post(API_URL + '/emailandphone', body);
  }

  logout = () => {
    localStorage.removeItem("user");
    console.log("logout");
  };

  register = async (first_name, last_name, password, type, email, phone) => {
      
      const resp = await axios.post(API_URL + "register", {
        first_name,
        last_name,
        password,
        type,
        email,
        phone,
      })
     
      return resp;
  
  }

  getUserById(id) {
    return axios.get(API_URL + '/' + id);
  }

  getCurrentUser() {
    return localStorage.getItem("user");
  }
}

export default new AuthService();
