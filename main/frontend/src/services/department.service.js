import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "http://localhost:5000/api/departments";

class DepartmentService {
  async getAllDepartments() {
    return await axios.get(API_URL + "/", { headers: authHeader() });
  }

  async getAllDepartmentsByLoc(loc_id) {
    return await axios.get(API_URL + "/" + loc_id, { headers: authHeader() });
  }

  
}

export default new DepartmentService();