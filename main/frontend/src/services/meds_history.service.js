import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "http://localhost:5000/api/meds_history";

class MedsHistory {
  async getAllMedsHistoryByUserId(id) {
    return await axios.get(API_URL + "/" + id, { headers: authHeader() });
  }

  

  async postMedsHistory(body){
    return await axios.post(API_URL + "/", body, {headers: authHeader() });
  }
}

export default new MedsHistory();