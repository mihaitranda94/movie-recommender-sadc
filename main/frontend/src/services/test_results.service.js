import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "http://localhost:5000/api/test_results";

class TestResultsService {
  async getAllTestResultsByUserId(id) {
    return await axios.get(API_URL + "/" + id, { headers: authHeader() });
  }

  

  async postTestResult(body){
    return await axios.post(API_URL + "/", body, {headers: authHeader() });
  }
}

export default new TestResultsService();