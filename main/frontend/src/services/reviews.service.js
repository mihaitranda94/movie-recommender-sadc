import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "http://localhost:5000/api/reviews";

class ReviewsService {
  async getAllReviewsById(id) {
    return await axios.get(API_URL + "/" + id, { headers: authHeader() });
  }

  async deleteReview(id) {
    return await axios.delete(API_URL + "/" + id, { headers: authHeader() });
  }

  

  async postReview(body){
    return await axios.post(API_URL + "/", body, {headers: authHeader() });
  }
}

export default new ReviewsService();
