import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "http://localhost:5000/api/location";

class LocationService {
  async getAllLocations() {
    return await axios.get(API_URL + "/", { headers: authHeader() });
  }

  async deleteLocation(id) {
    return await axios.delete(API_URL + "/" + id, { headers: authHeader() });
  }

  async updateLocation(body) {
    return await axios.put(API_URL + "/" + body.l_id, body, { headers: authHeader() });
  }

  
}

export default new LocationService();