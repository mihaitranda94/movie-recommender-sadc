import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "http://localhost:5000/api/bookings_requests";

class RequestService {
  async getAllRequests() {
    return await axios.get(API_URL + "/", { headers: authHeader() });
  }

  async getUserByNameAndEmail(first_name, last_name, email) {
    return await axios.post(
      API_URL + "/user_id",
      { first_name: first_name, last_name: last_name, email: email },
      { headers: authHeader() }
    );
  }
  async getDoctorByName(name) {
    return await axios.post(API_URL + "/doc_id", {name: name}, {
      headers: authHeader(),
    });
  }

  async postRequest(body) {
    return await axios.post(API_URL + "/", body, { headers: authHeader() });
  }

  async updateRequest(body) {
      return await axios.put(API_URL + "/", body, { headers: authHeader()});
    
  }
}

export default new RequestService();
