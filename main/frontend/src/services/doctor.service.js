import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "http://localhost:5000/api/doctors";

class DoctorService {
  async getAllDoctors() {
    return await axios.get(API_URL + "/", { headers: authHeader() });
  }

  async getDoctorById(id) {
    return await axios.get(API_URL + "/doctor/" + id, { headers: authHeader() });
  }

  async getAllDoctorsByDep(dep_id) {
    return await axios.get(API_URL + "/" + dep_id, { headers: authHeader() });
  }

  async getLocationByDoctorId(d_id){
    return await axios.get(API_URL + "/location/" + d_id, { headers: authHeader() });
  }

  async getDepartmentByDoctorId(d_id){
    return await axios.get(API_URL + "/department/" + d_id, { headers: authHeader() });
  }

  async updateDoctor(body) {
    return await axios.put(API_URL + "/" + body.d_id, body, { headers: authHeader() });
  }

  async deleteDoctor(d_id) {
    return await axios.delete(API_URL + "/" + d_id, { headers: authHeader() });
  }
}

export default new DoctorService();
