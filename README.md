The Movie Recommender System uses both content based filtering as well as user based filtering in order to express meaningful recommendations to the user. The interface logic is similar to that of Netflix, in which we have a number carousels for each type of recommendations. We have a cosine similarity recommendation based on the movies in cold start form, movies with the favourite people(actor/director), and movie recommendations based on keywords associated with the movie. 

Moreover, we also implemented a "People who watched this also watched..." for the movies in the cold start form.
