CREATE TABLE public.users (
    user_id SERIAL PRIMARY KEY,
    first_name text NOT NULL,
    last_name text NOT NULL,
    email text COLLATE pg_catalog."default" NOT NULL,
    password text COLLATE pg_catalog."default" NOT NULL
);

CREATE TABLE public.users_information (
    information_id SERIAL PRIMARY KEY,
    user_id integer NOT NULL,
    country_code text NOT NULL,
    age numeric NOT NULL,
    preferred_genre text [] NOT NULL,
    preferred_actors text [] NOT NULL,
    preferred_directors text [] NOT NULL,
    CONSTRAINT fk_users_info FOREIGN KEY(user_id) REFERENCES users(user_id)
);

CREATE TABLE public.notifications(
    notification_id SERIAL PRIMARY KEY,
    user_id integer  NOT NULL,
    type text NOT NULL,
    description text NOT NULL,
    CONSTRAINT fk_notifications FOREIGN KEY(user_id) REFERENCES users(user_id)
);

CREATE TABLE public.recommendations_genre (
    recommendation_gen_id SERIAL PRIMARY KEY,
    user_id integer NOT NULL,
    movie_id integer NOT NULL,
    CONSTRAINT fk_recommendations_genre FOREIGN KEY(user_id) REFERENCES users(user_id)
);

CREATE TABLE public.recommendations_similar_users (
    recommendation_sim_id SERIAL PRIMARY KEY,
    user_id integer NOT NULL,
    movie_id integer NOT NULL,
    CONSTRAINT fk_recommendations_similar_users FOREIGN KEY(user_id) REFERENCES users(user_id)
);

CREATE TABLE public.recommendations_country (
    recommendation_dem_id SERIAL PRIMARY KEY,
    user_id integer NOT NULL,
    movie_id integer NOT NULL,
    CONSTRAINT fkrecommendations_country FOREIGN KEY(user_id) REFERENCES users(user_id)
);

CREATE TABLE public.recommendations_content_similar (
    recommendation_dem_id SERIAL PRIMARY KEY,
    user_id integer NOT NULL,
    movie_id integer NOT NULL,
    CONSTRAINT fk_recommendations_content_similar FOREIGN KEY(user_id) REFERENCES users(user_id)
);

CREATE TABLE public.history_movies(
    history_movie_id SERIAL PRIMARY KEY,
    user_id integer NOT NULL,
    movie_id integer not null,
    watched boolean not null,
    CONSTRAINT fk_clicked_movies FOREIGN KEY(user_id) REFERENCES users(user_id)
);
