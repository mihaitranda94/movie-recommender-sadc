CREATE TABLE public.users (
    user_id SERIAL PRIMARY KEY,
    first_name text NOT NULL,
    last_name text NOT NULL,
    email text COLLATE pg_catalog."default" NOT NULL,
    password text COLLATE pg_catalog."default" NOT NULL
);

CREATE TABLE public.users_information (
    information_id SERIAL PRIMARY KEY,
    user_id integer NOT NULL,
    location text NOT NULL,
    age numeric NOT NULL,
    preferred_genre text [] NOT NULL,
    preferred_actors text [] NOT NULL,
    preferred_directors text [] NOT NULL,
    preferred_writers text [] NOT NULL,
    CONSTRAINT fk_users_info FOREIGN KEY(user_id) REFERENCES users(user_id)
);

CREATE TABLE public.notifications(
    notification_id SERIAL PRIMARY KEY,
    user_id integer  NOT NULL,
    type text NOT NULL,
    description text NOT NULL,
    CONSTRAINT fk_notifications FOREIGN KEY(user_id) REFERENCES users(user_id)
);

CREATE TABLE public.recommendations_genre (
    recommendation_gen_id SERIAL PRIMARY KEY,
    user_id integer NOT NULL,
    title_id text NOT NULL,
    CONSTRAINT fk_recommendations_genre FOREIGN KEY(user_id) REFERENCES users(user_id),
    CONSTRAINT fk_recommendations_genre_title FOREIGN KEY(title_id) REFERENCES titles(title_id)
);

CREATE TABLE public.recommendations_similar_users (
    recommendation_sim_id SERIAL PRIMARY KEY,
    user_id integer NOT NULL,
    title_id text NOT NULL,
    CONSTRAINT fk_recommendations_similar_users FOREIGN KEY(user_id) REFERENCES users(user_id),
    CONSTRAINT fk_recommendations_similar_users_title FOREIGN KEY(title_id) REFERENCES titles(title_id)
);

CREATE TABLE public.recommendations_demographic (
    recommendation_dem_id SERIAL PRIMARY KEY,
    user_id integer NOT NULL,
    title_id text NOT NULL,
    CONSTRAINT fk_recommendations_demographic FOREIGN KEY(user_id) REFERENCES users(user_id),
    CONSTRAINT fk_recommendations_demographic_title FOREIGN KEY(title_id) REFERENCES titles(title_id)
);

CREATE TABLE public.clicked_movies(
    clicked_movie_id SERIAL PRIMARY KEY,
    user_id integer NOT NULL,
    title_id text not null,
    CONSTRAINT fk_clicked_movies FOREIGN KEY(user_id) REFERENCES users(user_id),
    CONSTRAINT fk_clicked_movies_titles FOREIGN KEY(title_id) REFERENCES titles(title_id)
);

CREATE TABLE public.favourites(
    favourite_id SERIAL PRIMARY KEY,
    user_id int  NOT NULL,
    title_id text not null,
    CONSTRAINT fk_favourite FOREIGN KEY(user_id) REFERENCES users(user_id),
    CONSTRAINT fk_favourite_titles FOREIGN KEY(title_id) REFERENCES titles(title_id)
);

CREATE TABLE public.reviews(
    review_id SERIAL PRIMARY KEY,
    user_id integer NOT NULL,
    title_id text not null,
    rating numeric(4, 2) not null,
    CONSTRAINT fk_review FOREIGN KEY(user_id) REFERENCES users(user_id),
    CONSTRAINT fk_review_titles FOREIGN KEY(title_id) REFERENCES titles(title_id)
);