CREATE TABLE public.namebasics (
    name_id text COLLATE pg_catalog."default" NOT NULL,
    name text NOT NULL,
    birthyear text COLLATE pg_catalog."default" NOT NULL,
    deathyear text COLLATE pg_catalog."default" NOT NULL,
    primaryprofession text [] COLLATE pg_catalog."default" NOT NULL,
    knownfortitles text [] COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT nconst_pkey PRIMARY KEY (name_id)
);

CREATE TABLE public.titles (
    title_id text COLLATE pg_catalog."default" NOT NULL,
    ordering text NOT NULL,
    title text NOT NULL,
    region text NOT NULL,
    language text NOT NULL,
    types text [] NOT NULL,
    attributes text [] NOT NULL,
    isOriginalTitle text NOT NULL,
    CONSTRAINT title_id_pkey PRIMARY KEY (title_id)
);

CREATE TABLE public.title_basics (
    title_id text COLLATE pg_catalog."default" NOT NULL,
    title_type text NOT NULL,
    primary_title text NOT NULL,
    originalTitle text NOT NULL,
    isAdult text NOT NULL,
    startYear text NOT NULL,
    endYear text NOT NULL,
    runtime_minutes text NOT NULL,
    generes text [] NOT NULL,
    CONSTRAINT title_id2_pkey PRIMARY KEY (title_id),
    CONSTRAINT fk_titles_basics FOREIGN KEY(title_id) REFERENCES titles(title_id)
);

CREATE TABLE public.titles_crew (
    title_id text COLLATE pg_catalog."default" NOT NULL,
    directors text [] NOT NULL,
    writers text [] NOT NULL,
    CONSTRAINT title_id3_pkey PRIMARY KEY (title_id),
    CONSTRAINT fk_titles_crew FOREIGN KEY(title_id) REFERENCES titles(title_id)
);

CREATE TABLE public.titles_episodes (
    title_id text COLLATE pg_catalog."default" NOT NULL,
    parentConst text NOT NULL,
    season_number text NOT NULL,
    epsiode_number text NOT NULL,
    CONSTRAINT title_id4_pkey PRIMARY KEY (title_id),
    CONSTRAINT fk_titles_episodes FOREIGN KEY(title_id) REFERENCES titles(title_id)
);

CREATE TABLE public.titles_ratings (
    title_id text COLLATE pg_catalog."default" NOT NULL,
    average_rating text NOT NULL,
    number_votes text NOT NULL,
    CONSTRAINT title_id5_pkey PRIMARY KEY (title_id),
    CONSTRAINT fk_titles_ratings FOREIGN KEY(title_id) REFERENCES titles(title_id)
);

CREATE TABLE public.titles_principals (
    title_id text COLLATE pg_catalog."default" NOT NULL,
    name_id text NOT NULL,
    ordering text NOT NULL,
    category text NOT NULL,
    job text NOT NULL,
    character text NOT NULL,
    CONSTRAINT title_id6_pkey PRIMARY KEY (title_id),
    CONSTRAINT fk_titles_principals FOREIGN KEY(name_id) REFERENCES namebasics(name_id),
    CONSTRAINT fk_titles_principals2 FOREIGN KEY(title_id) REFERENCES titles(title_id)
);