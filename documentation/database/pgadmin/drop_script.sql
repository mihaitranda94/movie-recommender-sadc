DROP TABLE IF EXISTS public.namebasics CASCADE;

DROP TABLE IF EXISTS public.notifications CASCADE;

DROP TABLE IF EXISTS public.recommendation_genre CASCADE;

DROP TABLE IF EXISTS public.reviews CASCADE;

DROP TABLE IF EXISTS public.title_basics CASCADE;

DROP TABLE IF EXISTS public.titles CASCADE;

DROP TABLE IF EXISTS public.titles_crew CASCADE;

DROP TABLE IF EXISTS public.titles_episodes CASCADE;

DROP TABLE IF EXISTS public.titles_ratings CASCADE;

DROP TABLE IF EXISTS public.clicked_movies CASCADE;

DROP TABLE IF EXISTS public.favourites CASCADE;

DROP TABLE IF EXISTS public.form CASCADE;

DROP TABLE IF EXISTS public.notifications CASCADE;

DROP TABLE IF EXISTS public.recommendations_genre CASCADE;

DROP TABLE IF EXISTS public.recommendations_demographic CASCADE;

DROP TABLE IF EXISTS public.recommendations_similar_users CASCADE;

DROP TABLE IF EXISTS public.reviews CASCADE;

DROP TABLE IF EXISTS public.users CASCADE;

DROP TABLE IF EXISTS public.users_information CASCADE;

DROP TABLE IF EXISTS public.titles_ratings CASCADE;

DROP TABLE IF EXISTS public.titles_principals CASCADE;